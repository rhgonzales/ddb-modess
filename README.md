# Courage Unlocked

Courage Unlocked - DDB: Tribal Worldwide

```
#!php

git clone git@bitbucket.org:rhgonzales/ddb-modess.git
git pull origin master
```


Download Database SQL file from following URL
```
#!php
https://bitbucket.org/rhgonzales/ddb-modess/downloads/
```
Run composer to install PHP Mailer
```
#!php
composer install
```

## Linked Jira Project:

https://apdgroup.atlassian.net/secure/RapidBoard.jspa?rapidView=156&projectKey=DDBTWM
