<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms extends CI_Controller {

    public function __construct() {
        parent::__construct();
       
        $this->load->library('apd_template');
        $this->load->model('cms_model');
        $this->load->helper('url');
    }

    public function index() {



        $identifier =  $this->uri->segment(1);

        $data['mechanics'] = array('title'=> 'Mechanics','path'=>'static_pages/mechanics','body_class' => 'page-mechanics');
        $data['terms-and-conditions'] = array('title'=> 'Terms And Conditions','path'=>'static_pages/terms_and_conditions','body_class' => 'page-mechanics');
        $data['privacy-policy'] = array('title'=> 'Privacy Policy','path'=>'static_pages/privacy_policy','body_class' => 'page-mechanics');
        $data['participating-products'] = array('title'=> 'Participating Products','path'=>'static_pages/participating-products','body_class' => 'page-mechanics');
        $data['prizes'] = array('title'=> 'Prizes','path'=>'static_pages/prizes','body_class' => 'page-mechanics');

        //$data['cms'] = $this->cms_model->get_cms_page($identifier);
        $info['login_url'] = $this->facebook->getLoginUrl(array(
            'redirect_uri' => site_url(),
            'scope' => array("email") // permissions here
        ));
        $info['take_photo'] = base_url('photo');
        $info['home_link'] = base_url();
        $info['body_class'] = $data[$identifier]['body_class'];
        $body_tpl = $this->load->view('body/frontend/'.$data[$identifier]['path'], $info, true);
        $this->apd_template->load_frontend_template($body_tpl,$data[$identifier]['title']);
    }  
    
}