<?php

Class Filters extends CI_Controller{

    public function __construct(){

        parent::__construct();

        $this->load->library('apd_template');
    }

    public function index(){

        $title = "Filters Page";
        $data['body_class'] = "page-filter";
        $data['selfie_link'] = base_url('photo');
        $body_tpl = $this->load->view('body/frontend/filters',$data,TRUE);
        $this->apd_template->load_frontend_template($body_tpl, $title);
    }

}