<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Gallery extends CI_Controller {

    protected $_items_per_page = 0;

    protected $_data = array();

    protected $_collection;

    protected $_total_row;

    protected $_user_id;

    public function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->model('member_model');
        $this->load->helper('apd_helper');
        $this->load->library('apd_template');
        $this->load->model('gallery_model');
        $this->load->library('facebook');

        $this->_items_per_page = 15;

        $this->_user_id = $this->session->userdata('user_id');
        
    }

    public function index(){

        $info['home_link'] = base_url();
        $info['photo_link'] = base_url('photo');
        $info['all_entries'] = base_url('gallery/all-entries');
        $info['body_class'] = "page-gallery";
        if($this->_user_id){
            $info['my_entries'] = base_url('gallery/my-entries');
        }

        $title = "Gallery";
        $body_tpl = $this->load->view('body/frontend/gallery',$info,true);
        $this->apd_template->load_frontend_template($body_tpl, $title);
    }

    /**
     * List all entries
     */
    public function all_entries($page_title = 'page',$page_num = 1){

        if($page_title == "page"){

            $offset = ($page_num - 1) * ($this->_items_per_page);
            $this->_total_row = $this->gallery_model->get_active_entries()->num_rows();

            $this->_collection =  $this->gallery_model->get_active_entries($this->_items_per_page,$offset);

            echo $this->get_json_data($page_title,$page_num,'all_entries');

        }else{
            echo show_error('Invalid parameters');
        }

    }

    /**
     * List all Users Entries
     */
    public function my_entries($page_title = 'page',$page_num = 1){

        if($page_title == "page"){
            if($this->_user_id){

                $offset = ($page_num - 1) * ($this->_items_per_page);
                $this->_total_row = $this->gallery_model->get_entries_by_userid($this->_user_id)->num_rows();

                $this->_collection = $this->gallery_model->get_entries_by_userid($this->_user_id,$this->_items_per_page,$offset);
                echo $this->get_json_data($page_title,$page_num,'my_entries');
            }else{
                echo show_error('No User Selected');
            }
        }else{
           echo show_error('Invalid parameters');
        }
    }

    protected function get_json_data($page_title,$page_num,$uri = 'all_entries'){


        $this->load->model('image_model');
        $result = array();
        $last_page  = ceil($this->_total_row/$this->_items_per_page);
        if($page_num < $last_page){
            $next_page = $page_num + 1;
            $result['next_page_url'] = base_url("gallery/{$uri}/{$page_title}/{$next_page}");
        }
        $result['total_row'] = $this->_total_row;
        $result['current_page'] = $page_num;
        $result['item_per_page'] = $this->_items_per_page;
        $result['data'] = array();
        foreach ($this->_collection->result() as $item) {
            $result['data'][] = array(
                'entry_id' => $item->entry_id,
                'user_id' => $item->user_id,
                'entry_link' => base_url('gallery/view/detail/' . $item->entry_id),
                'image' => $this->image_model->get_thumbnail_path($item->image),
                'status' => $item->status,
                'title' => $item->title,
                'description' => $item->description
            );
        }

        $this->output->set_content_type('application/json');
        return json_encode($result);
    }

    /**
     * @param null $entry_id
     * @param null $type
     */
    public function view($type = null,$entry_id = null){

        $user = $this->facebook->getUser();
        if(is_null($type) || is_null($entry_id)){
            return show_404();
        }
        $this->load->model('image_model');
        $query = $this->gallery_model->get_entry_by_id($entry_id)->row();

        if(empty($query) || !$query->status){
            return show_404();
        } 
        
        $query->fb_image = $this->image_model->get_resized_path($query->image);
        $query->image = $this->image_model->get_400x400_path($query->image);
        $query->thumbnail_image = $this->image_model->get_thumbnail_path($query->image);

        $data['body_class'] = "page-gallery";
        $data['entry'] = $query;
        $data['mechanics_link'] = base_url('mechanics');
        $data['home_link'] = base_url();
        $page_url = base_url(uri_string());
        $title = config_item('share_title');
        $description = config_item('share_description');
        $text = config_item('share_text');
        $hashtags = config_item('share_hashtags');
        $caption = config_item('share_caption');
        $extra_headers = array('url'=>$page_url,'title'=>$title,'image'=>$query->fb_image,'description'=>$description,'hashtags'=>$hashtags,'text'=>$text,'caption'=>$caption);
        $title = "Entry Page";    
        $share_data['sharer'] = $this->facebook->get_fb_share_url($extra_headers);
        $share_data['download_link'] = '/gallery/download/'.$entry_id;
        $share_data['gallery_link'] =  base_url('gallery');
        $share_data['upload_selfie'] = base_url('photo');
        $share_data['data'] = $extra_headers;
        $login = $this->session->userdata('member_login');        
        if(!$login) {            
        $share_data['login_link'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url()."/?refer=$page_url",
                'scope' => array("email") // permissions here
            ));
        $data['login_link'] = $share_data['login_link'];
        }
                        
        if($type=='detail')
            $data['share_tpl'] = $this->load->view('tpls/frontend/partials/gallery_detail_tpl',$share_data,true); 
        else
            $data['share_tpl'] = $this->load->view('tpls/frontend/partials/gallery_share_tpl',$share_data,true);

        $body_tpl = $this->load->view('body/frontend/view',$data,true);
        $this->apd_template->load_frontend_template($body_tpl, $title, $extra_headers);

    }
    
    public function download($entry_id = null) {
        if(is_null($entry_id)){
            return show_404();
        }
                
        $query = $this->gallery_model->get_entry_by_id($entry_id)->row();        
        $image = $query->image;
        $attachment_location = 'media/images/base/'.$image;              
        
        if (file_exists($attachment_location)) {            
            header('Content-Description: File Transfer');
            header('Content-Type: image/jpeg');
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:".filesize($attachment_location));
            header("Content-Disposition: attachment; filename=$image");            
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');

            readfile($attachment_location);
            die();
        } else {
            die("Error: File not found.");
        }
        
        
    }

}