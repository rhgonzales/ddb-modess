<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('member_model');
        $this->load->helper('apd_helper');
        $this->load->library('apd_template');
    }

    public function index() {

        $user = $this->facebook->getUser();
        $code = $this->input->get('code');

        if($code && !$user){
            $fb_url =  $this->facebook->getLoginUrl();
            redirect($fb_url);
        }

        if ($user):

            $user_profile = $this->facebook->getUser();
            $data['user_profile'] = $user_profile;
            $this->session->set_userdata(array("fb_id" => $user_profile['id']));

        else:
            $this->facebook->destroySession();

        endif;

        if ($user && $code):

            $member_details = $this->member_model->get_member_by_fb($user_profile['id']);

            if (count($member_details) <= 0):

                $title = 'Login With Facebook';
                $data['terms_url'] = base_url('terms-and-conditions');
                $data['provinces'] = $this->get_provinces();
                $data['months'] = get_months();
                $data['action'] = site_url('home/save');
                $data['body_class'] = "page-registration";
                $body_tpl = $this->load->view('body/frontend/register', $data, TRUE);

            else:

                foreach ($member_details as $member => $detail):
                    $this->session->set_userdata(array($member => $detail));
                endforeach;
                $this->session->set_userdata(array('member_login' => 'true'));

                redirect('photo');

            endif;

        else:

            if(check_if_logged_in()){
                $data['login_url'] = 'photo';
            }else{
                $data['login_url'] = $this->facebook->getLoginUrl();
            }
 
            $data['body_class'] = "home-nav page-index";
            $data['nav_class'] = "home-nav";
            $title = 'Home';
            $body_tpl = $this->load->view('body/frontend/login', $data, TRUE);

        endif;

        $this->apd_template->load_frontend_template($body_tpl, $title);
    }

    public function success_registration(){

        if($this->session->flashdata('is_success')){
            $data['product_page'] = base_url('participating-products');
            $data['take_photo'] = base_url('photo');
            $data['home_link'] = base_url();
            $data['body_class'] = "page-registration";
            $title = 'Success Registration';
            $body_tpl = $this->load->view('body/frontend/success_registration', $data, TRUE);
            $this->apd_template->load_frontend_template($body_tpl, $title);
        }else{
            redirect('/');
        }
    }

    public function save() {

        $fb_id = $this->session->userdata('fb_id');

        //check user if already registred
        $member_details = $this->member_model->get_member_by_fb($fb_id);
        if(!empty($member_details)){
            redirect();
        }

        if (isset($fb_id) && count($this->input->post()) > 0):
            foreach($this->input->post() as $key=>$value):
                $member[$key] =  $this->security->xss_clean($value);
            endforeach;

            $member['fb_id'] = $this->session->userdata('fb_id');

            $member['dob'] = date('Y-m-d',strtotime($member['dob']));
            unset($member['month']);
            unset($member['day']);
            unset($member['year']);
            unset($member['refer']);
            unset($member['terms_and_condition']);

            $member['agree_to_terms'] = isset($_POST['agree_to_terms']);
            $id = $this->member_model->save_member($member);
            $this->session->set_userdata(array('member_login' => 'true','user_id'=>$id));
            if($_POST['refer'] !=''){
                redirect($_POST['refer']);
            }else{
                $this->session->set_flashdata('is_success',1);
                redirect('home/success_registration');
            }


        else:
            redirect('home/index/result/error');

        endif;
    }

    public function logout() {

        // Logs off session from website
        $this->facebook->destroySession();
        $this->session->sess_destroy();
        // Make sure you destory website session as well.

        redirect('home');
    }

    /**
     * List all  Philippines provinces
     * @return array
     */
    protected function get_provinces(){

        $csv_loc = FCPATH . 'assets/csv/provinces.csv';
        $record = array();

        if(($handle = fopen($csv_loc,"r")) != false){
            while(($data = fgetcsv($handle,8000,",")) != false){
                $record[] = $data[0];
            }
        }
        return $record;
    }

}
