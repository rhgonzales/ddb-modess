<?php

class Photo extends CI_Controller{

    protected $_user_id;

    protected $_current_entry;
    /**
     * Photo constructor.
     */
    public function __construct(){

        parent::__construct();

        $this->load->model('member_model');
        $this->load->helper('apd_helper');
        $this->load->library('apd_template');
        $this->load->model('gallery_model');
        $this->load->library('facebook');

        $this->_user_id = $this->session->userdata('user_id');

        if(!check_if_logged_in()){
           redirect($this->apd_template->_get_join_now_link());
        }


    }

    /**
     * Default page
     */
    public function index(){

        $title = 'Take Photo';
        $data['body_class'] = "page-take-photo";
        $body_tpl = $this->load->view('body/frontend/photo', $data, TRUE);

        $this->apd_template->load_frontend_template($body_tpl, $title);
    }

    public function capture(){

        $post = $this->input->post();

        $return_data = array();
        $security_token = array('auth_token_name' => $this->security->get_csrf_token_name(),
                                'auth_token_hash' => $this->security->get_csrf_hash());

        //set default value
        $title = $this->input->post('title');
        $description  = $this->input->post('description');
        $image_base64 = $this->input->post('image');

        $dataJson  = json_decode($this->input->post('data'),true);
        $filter_used  = $dataJson['frame']['name'];
        $image_type   = str_replace('image/','',$dataJson['imageFinalType']);

        $this->load->model('image_model');

        if($image_base64){

            // save image to server
            $image_type = !is_null($image_type) ? $image_type : 'jpg';
            $image_name = time() . "_{$this->_user_id}";
            $result = $this->image_model->temp_image($image_base64,$image_name,$image_type);

            if($this->image_model->is_image_exist($result->image_path)){

                //resize image
                $this->image_model->save($result);

                $data = array(
                    'user_id' => $this->_user_id,
                    'title' => $title,
                    'filter_used' => $filter_used,
                    'description' => $description,
                    'image' => $result->image_name,
                    'created_date' => date('Y-m-d H-i-s'),
                    'status' => 1 
                );
                // save image to database
                $return_data = $this->gallery_model->save($data);
                $return_data['data'] = $this->_get_entry_data($return_data['entry_id']);
                if($return_data['code'] == 0){
                    $return_data['error'] = 0;
                    $this->_send_mail($return_data['data']);
                }
            }else{
                $return_data = array('code' => '1', 'message' => 'Temporary Image not exist','error'=> 1);
            }

        }else{
            $return_data = array('code' => '1', 'message' => 'Invalid data','error'=> 1);
        }

        $this->output->set_content_type('application/json');
        echo json_encode(array_merge($return_data,$security_token));

    }

    private function _get_entry_data($entry_id){

        $data = array();
        $this->load->model('image_model');

        if($entry_id){

            $query = $this->gallery_model->get_entry_by_id($entry_id)->row();
            $this->_current_entry = $query;
            $query->fb_image = $this->image_model->get_resized_path($query->image);
            $query->image = $this->image_model->get_base_path($query->image);
            $query->thumbnail_image = $this->image_model->get_thumbnail_path($query->image);

            $page_url = base_url('gallery/view/detail/' . $entry_id);
            $title = config_item('share_title');
            $description = config_item('share_description');
            $text = config_item('share_text');
            $hashtags = config_item('share_hashtags');
            $caption = config_item('share_caption');
            $extra_headers = array('url'=>$page_url,'title'=>$title,'image'=>$query->fb_image,'description'=>$description,'hashtags'=>$hashtags,'text'=>$text,'caption'=>$caption);
   
            $data['entry_id'] = $entry_id;
            $data['email'] = $query->email;
            $data['first_name'] = $query->first_name;
            $data['last_name'] = $query->last_name;
            $data['image_url'] = $query->image;
            $data['view_details_url'] = $page_url;
            $data['home_link'] = base_url();
            $data['fb_share_url'] = html_entity_decode($this->facebook->get_fb_share_url($extra_headers));
            $data['download_link'] = base_url('/gallery/download/'.$entry_id);
            $data['twitter_share_url'] = html_entity_decode("https://twitter.com/share?url={$page_url}&hashtags={$hashtags}&text={$text}");
        }

        return $data;
    }

    private function _send_mail($entry){

        if(empty($entry))
            return;
        
        $this->load->model('mailer');
        $this->mailer->setFrom(config_item('mailer_from_email'),config_item('mailer_from_name'));
        $this->mailer->addAddress($entry['email']);
        $this->mailer->addBCC(config_item('mailer_bcc'));
        $this->mailer->isHTML(true);

        $val['data'] = $entry; 
        // Set email format to HTML
        $this->mailer->Subject = 'Courage Unlocked Promo: Your Entry Has Been Received!';
        $this->mailer->Body = $this->load->view('email_template/new_entry', $val, true);;


        if (!$this->mailer->send()) {
            return  'Mailer Error: ' . $this->mailer->ErrorInfo;
        } else {
            return 'Message has been sent';
        }
    }

}
