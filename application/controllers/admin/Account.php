<?php

Class Account extends CI_Controller{

    protected $_user_id;
    /**
     * Account constructor.
     */
    public function __construct(){

        parent::__construct();
        $this->load->model('user');
        $this->load->model('admin_session');

    }

    public function index(){

        $info['user'] = $this->_get_data();

        $data['header_title'] = "Account";
        $data['title'] = "Administrator Dashboard";
        $data['content'] = $this->load->view('admin/account',$info,true);
        $this->load->view('admin/template',$data);
    }

    /**
     * Update User Account
     */
    public function update(){

        $post = $this->input->post();
        $data = array();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('first_name', 'Firstname', 'required');
        $this->form_validation->set_rules('last_name', 'Lastname', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if((bool)$this->input->post('change_password')){
            $this->form_validation->set_rules('password', 'Password', 'required');
            $data['password'] = md5($post['password']);
        }

       if($this->form_validation->run() == FALSE){

           $this->session->set_flashdata('alert_danger', validation_errors());

       }else{
           try{
               $data['username'] = $post['username'];
               $data['first_name'] = $post['first_name'];
               $data['last_name'] = $post['last_name'];
               $data['email'] = $post['email'];

               $this->user->update($data,$this->admin_session->get_user_id());
               $this->session->set_flashdata('alert_success', 'Successfully saved!');

           }catch (Exception $e){

               $this->session->set_flashdata('alert_danger', $e->getMessage());
           }
       }

        redirect('admin/account');
    }

    /**
     * @return mixed
     */
    private function _get_data(){
        return $this->user->load_by_id($this->admin_session->get_user_id());
    }
}