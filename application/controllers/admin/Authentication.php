<?php

class Authentication extends CI_Controller{

    public function __construct(){
        parent::__construct();

         $this->load->model('user');

    }

    /**
     *  Default Login view
     */
    public function index(){

        $data['title'] = "Administrator Login";
        $this->load->view('admin/login',$data);
    }

    /**
     * Admin Account Validation
     */
    public function login(){

        $username = $this->input->post('username',TRUE);
        $password = $this->input->post('password',TRUE);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('alert_danger', validation_errors());
            redirect('admin');

        }else{

            $this->user->authenticate($username,$password);
            if($this->user->is_login()){

                $this->load->model('admin_session');
                $user_data = $this->user->get_userdata();
                $this->admin_session->add_user_session($user_data);

                redirect('admin/gallery');

            }else{
                $this->session->set_flashdata('alert_danger', 'Invalid login account');
                redirect('admin');
            }
        }
    }

    public function logout(){
        $this->output->set_content_type('application/json');
        $this->session->unset_userdata('logged_in');
        redirect('admin');
    }

    public function forgotpassword_submit(){

        $email = $this->input->post('email');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('alert_danger', validation_errors());
            redirect('admin/authentication/forgotpassword');

        }else {
            $this->load->library('encryption');
            $this->encryption->initialize(array('driver' => 'mcrypt'));
            $generated_key = bin2hex($this->encryption->create_key(16));

            //Add Auth Key to User
            $user = $this->user->load_admin_by_email($email);

            if(count($user) <= 0){
                $this->session->set_flashdata('alert_danger', "No record found on {$email}.");
                redirect('admin/authentication/forgotpassword');
            }

            $this->user->update(array('auth_key' => $generated_key), $user->user_id);
            $data['email'] = $user->email;
            $data['first_name'] = $user->first_name;
            $data['param'] = "?auth_key={$generated_key}&email={$user->email}&user_id={$user->user_id}";

            $this->load->model('mailer');
            $this->mailer->setFrom(config_item('mailer_from_email'),config_item('mailer_from_name'));
            $this->mailer->addAddress($user->email);                // Name is optional
            $this->mailer->isHTML(true);

            // Set email format to HTML
            $this->mailer->Subject = 'Reset Password';
            $this->mailer->Body = $this->load->view('email_template/forgot_password', $data, true);;


            if (!$this->mailer->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $this->mailer->ErrorInfo;
            } else {
                echo 'Message has been sent';
            }

            $this->session->set_flashdata('alert_success', 'Password reset email has been sent.');
            redirect('admin');
        }
    }

    /**
     * Reset Password submit
     */
    public function resetpassword_submit(){

        $post = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('new_password', 'New Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');

        if($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('alert_danger', validation_errors());
            redirect($post['current_url']);
        }

        if($post['new_password'] != $post['confirm_password']){
            $this->session->set_flashdata('alert_danger', 'New password and confirmation password does not match.');
            redirect($post['current_url']);
        }

        if($this->form_validation->run() != FALSE) {

            $user = $this->user->load_admin_by_email($post['email']);

            if($user->auth_key == $post['auth_key']){
                $this->user->update(array('auth_key' => '','password'=>MD5($post['new_password'])), $user->user_id);

                $this->session->set_flashdata('alert_success', 'New password has been saved.');
                redirect('admin');
            }else{
                $this->session->set_flashdata('alert_danger', 'Invalid authentication.');
                redirect('admin');
            }
        }
    }

    /**
     * Reset Password Page
     */
    public function resetpassword(){

        $data['title'] = "Reset Password";
        $this->load->view('admin/reset_password_form',$data);
    }

    /**
     * Forgot Password Page
     */
    public function forgotpassword(){

        $data['title'] = "Forgot Password";
        $this->load->view('admin/forgot_password',$data);
    }

}
