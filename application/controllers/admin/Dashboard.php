<?php


class Dashboard extends CI_Controller {

    /**
     * Dashboard constructor.
     */
    public function __construct(){

        parent::__construct();
        $this->load->model('admin_session');
    }

    /**
     *  Default Login view
     */
    public function index(){

        $data['header_title'] = "Dashboard";
        $data['title'] = "Administrator Dashboard";
        $data['content'] = $this->load->view('admin/dashboard',true,true);
        $this->load->view('admin/template',$data);
    }
}