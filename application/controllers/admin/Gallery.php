<?php


class Gallery extends CI_Controller{

    /**
     * @var int
     */
    protected $total_per_page = 18;

    protected $_total_row = 0;

    protected $_status;

    public function __construct(){

        parent::__construct();

        $this->load->model('user');
        $this->load->model('admin_session');
        $this->load->model('gallery_model');

        $this->load->helper('option');

        $this->_status = $this->session->userdata("filter_status");

    }

    public function index(){

        $current_page = $this->uri->segment(4,1);

        $offset = ($current_page - 1) * $this->total_per_page;
        $this->_total_row = $this->gallery_model->get_entries()->num_rows();
        $this->_pagination( $this->_total_row,$this->total_per_page,base_url('admin/gallery/page'));

        $info['filter_status'] = isset($this->_status) ? $this->_status : 2;
        $info['entries'] = $this->gallery_model->get_entries($this->total_per_page,$offset);
        $info['total_entries'] = $this->_total_row;

        $data['header_title'] = "Gallery";
        $data['title'] = "Gallery";
        $data['content'] = $this->load->view('admin/gallery',$info,true);
        $this->load->view('admin/template',$data);
    }


    public function _remap($method){
        $param_offset = 2;
        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }
        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    /**
     * Update Status
     */
    public function update_status(){

        $post = $this->input->post();
        if(isset($post['status']) && isset($post['entry_id'])){

           $result =  $this->gallery_model->save(array('status'=> $post['status']),$post['entry_id']);
        }else{
            $result = array('code'=> 1,'message'=> 'Error on updating status.');
        }

        $this->output->set_content_type('application/json');

        $result['csrf_token'] = $this->security->get_csrf_hash();
        echo  json_encode($result);
    }

    // Set filter Status
    public function filter_status(){

        $status = $this->input->post('filter_status');
        $this->session->set_userdata("filter_status",$status);
        $result['csrf_token'] = $this->security->get_csrf_hash();

        $this->output->set_content_type('application/json');
        echo  json_encode($result);
    }

    /**
     * Export Entries to CSV
     */
    public function export_to_csv(){

        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $filename = "export_entries_" . time() . ".csv";
        $result = $this->gallery_model->get_entries_for_csv();

        $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
        force_download($filename, $data);
    }

    /**
     * Generate Pagination
     * @param int $total_row
     * @param int $num_per_page
     * @return mixed
     */
    private function _pagination($total_row = 0,$num_per_page = 10,$url){
        $this->load->library('pagination');

        $config['base_url'] = $url;
        $config['total_rows'] = $total_row;
        $config['per_page'] = $num_per_page;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';

        // If you want to wrap the "go to first" link
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

// If you want to wrap the "go to last" link
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="">';
        $config['last_tag_close'] = '</li>';

// If you want to wrap the next link
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="">';
        $config['next_tag_close'] = '</li>';

// If you want to wrap the previous link
        $config['prev_link'] = 'Last';
        $config['prev_tag_open'] = '<li class="">';
        $config['prev_tag_close'] = '</li>';

// Wrap/style active link
        $config['cur_tag_open'] = '<li class="active "><a>';
        $config['cur_tag_close'] = '</a></li>';

// Wrap the 'digit' link.
        $config['num_tag_open'] = '<li class="">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
    }


}