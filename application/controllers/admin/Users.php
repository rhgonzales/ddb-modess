<?php

Class Users extends CI_Controller{

  
    /**
     * Users constructor.
     */
    public function __construct(){

        parent::__construct();
        $this->load->model('user');
        $this->load->model('admin_session');

    }

    public function index(){

        $info['user_list'] = $user_list = $this->user->user_collection($this->user->get_default_user_type());

        $data['header_title'] = "Users List";
        $data['title'] = "Administrator Dashboard";
        $data['content'] = $this->load->view('admin/users/list',$info,true);
        $this->load->view('admin/template',$data);
    }

    /**
     * View User's Profile
     * @param $user_id
     */
    public function view($user_id){

        if($user_id){

            $user =$this->user->load_by_id($user_id);
            $data['header_title'] = "{$user->first_name} {$user->last_name}";
            $info['user'] = $user;
            $data['title'] = "Administrator Dashboard";
            $data['content'] = $this->load->view('admin/users/form',$info,true);
            $this->load->view('admin/template',$data);

        }else{
            redirect('admin/users');
        }
    }

    public function update(){

        $post = $this->input->post();
        $data = array();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if((bool)$this->input->post('change_password')){
            $this->form_validation->set_rules('password', 'Password', 'required');
            $data['password'] = md5($post['password']);
        }

        if($this->form_validation->run() == FALSE){

            $this->session->set_flashdata('alert_danger', validation_errors());

        }else{
            try{
                $data['first_name'] = $post['first_name'];
                $data['last_name'] = $post['last_name'];
                $data['email'] = $post['email'];
                $data['dob'] = $post['dob'];
                $data['gender'] = $post['gender'];
                $data['address'] = $post['address'];

                $this->user->update($data,$post['user_id']);
                $this->session->set_flashdata('alert_success', 'Successfully saved!');

            }catch (Exception $e){

                $this->session->set_flashdata('alert_danger', $e->getMessage());
            }
        }

        redirect('admin/users/view/'.$post['user_id']);
    }

    
}