<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_months'))
{
    function get_months()
    {
        $months = array("01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June","07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");
        return $months;
    }
    
    function display_field($object, $field) {
        if(!isset($object[$field]))
        $object[$field] = '';
        
        return $object[$field];
            
    }
    
    function check_if_logged_in() {
        $ci = & get_instance();     
        $fb_id = $ci->session->userdata('member_login');
        
        return isset($fb_id)? true : false;
        
    }
        
}

