<?php

function option_yesno($value){

    $option = array(0 => 'No', 1 => 'Yes');

    if(array_key_exists($value,$option)){
        return $option[$value];
    }

    return false;
}

function option_status($value){

    $option = array(0 => 'Not Active', 1 => 'Active');

    if(array_key_exists($value,$option)){
        return $option[$value];
    }

    return false;
}