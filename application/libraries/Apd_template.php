<?php

class Apd_template {

    public function __construct() {       
         $this->_ci = & get_instance();         
    }

    function load_frontend_template($body, $title, $extra_header = '') {
                
        $this->_ci->load->helper('apd_helper');
        $fb_id = $this->_ci->session->userdata('fb_id');

        $logout_link = check_if_logged_in() ? base_url('home/logout') : '';
        $link['logout_link'] = $logout_link;      
        $link['join_now_link'] = isset($fb_id) ? base_url('photo') : $this->_get_join_now_link();
        $link['social_links'] = $this->social_media_url();
        $data['title'] = $title; 
                        
        if($extra_header!='')        
        $data['extra_headers'] = $this->_ci->load->view('tpls/frontend/partials/extra_header_tpl', $extra_header, true);
        $data['nav_menu'] = $this->_ci->load->view('body/frontend/home_nav',$link,true);
        $data['header1'] =  $this->_ci->load->view('tpls/frontend/partials/header_tpl', $link, true);
        $data['body'] = $body;
        $data['footer'] =  $this->_ci->load->view('tpls/frontend/partials/footer_tpl', $link, true);        
        
        $this->_ci->load->view('tpls/frontend/main_tpl.php', $data, false);
    }
 
    /**
     * @return mixed
     */
    public function _get_join_now_link(){

        return $this->_ci->facebook->getLoginUrl(array(
            'redirect_uri' => site_url(),
            'scope' => array("email") // permissions here
        ));
    }

    /**
     * @return array
     */
    public function social_media_url(){
        return array(
                'modess' => 'https://www.facebook.com/modessangelsofficial',
                'carefree' => 'https://www.facebook.com/CarefreePH/',
                'clean_clear' => 'https://www.facebook.com/cleanclearphilippines/',
            );
    }
}
