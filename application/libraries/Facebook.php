<?php
class Facebook extends \Facebook\Facebook
{
    /**
     * @var \Facebook\Helpers\FacebookRedirectLoginHelper
     */
    private $helper;

    /**
     * @var null
     */
    private $user = null;

    /**
     * Facebook constructor.
     */
    public function __construct() {
        $this->_ci =& get_instance();
        $this->_ci->load->config('facebook');
        parent::__construct([
            'app_id' => $this->_ci->config->item('appId'),
            'app_secret' => $this->_ci->config->item('secret'),
            'default_graph_version' => 'v2.8'
        ]);


        $this->helper = $this->getRedirectLoginHelper();

    }

    /**
     * Return FB Login URL
     * @return string
     */
    public function getLoginUrl() {
        return $this->helper->getLoginUrl( site_url() , ['email'] );
    }

    /**
     * Return User Data
     * @return bool|\Facebook\GraphNodes\GraphUser|null
     */
    public function getUser()
    {
        if (!empty($this->user)) {
            return $this->user;
        }
        try {
            $access_token = $this->helper->getAccessToken();
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            return false;
        }

        if( empty( $access_token ) ) {
            return false;
        }
        $this->user = $this->get('/me?fields=first_name,last_name,email', $access_token->getValue())->getGraphUser();

        return $this->user;
    }

    /**
     * FB Sharer URL
     * @param array $content
     * @return string
     */
    public function get_fb_share_url($content = array()){
        $url = '';
        if(!empty($content)){
            $app_id = $this->getApp()->getId();
            $url ="https://www.facebook.com/dialog/feed?app_id=$app_id&display=popup&amp;caption=".$content['caption']."&amp;link=".$content['url']."&amp;redirect_uri=".$content['url']."&amp;description=".str_replace('#','%23',$content['description'])."&amp;picture=".$content['image'];
        }
        return $url;
    }

    function api( $params ){
        return false;
    }

    function destroySession(){
        return false;
    }
}