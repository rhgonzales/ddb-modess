<?php

Class Admin_session extends CI_Model{

    /**
     * @var
     */
    protected $_user;

    /**
     * @var
     */
    protected $_is_user_confirmed;

    /**
     * @var
     */
    protected $_user_id;



    /**
     * User_session constructor.
     */
    public function __construct(){

        parent::__construct();

        $user_session = $this->session->userdata();
        if(!isset($user_session['logged_in']) && !$user_session['logged_in']){
            redirect('admin');
        }

        //init user ID
        $this->_user_id = $this->session->userdata('user_id');
    }

    /**
     * @param $user
     * @return $this
     */
    public function set_user($user){
        $this->_user = $user;
        return $this;
    }

    public function get_user_id(){
        return $this->_user_id;
    }

    /**
     * @param $user
     */
    public function add_user_session($user){

        $user_data = array(
            'usertype'  => $user->usertype,
            'user_id'   => $user->user_id,
            'username'  => $user->username,
            'email'     => $user->email,
            'name'      => ucwords($user->first_name . ' ' . $user->last_name),
            'logged_in' => TRUE
        );

        $this->session->set_userdata($user_data);
    }
}