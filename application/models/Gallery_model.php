<?php


class Gallery_model extends CI_Model{

    const GALLERY_TABLE = 'tbl_entries';

    protected $_config = array();

    /**
     * Gallery_model constructor.
     */
    public function __construct(){

        parent::__construct();

    }

    /**
     * Insert Images to tbl_entries
     * @param $data
     * @param null $entry_id
     * @return mixed
     */
    public function save($data,$entry_id = null){

        $result = array();
        $this->db->set($data);

        if(is_null($entry_id)) {
            $this->db->insert(self::GALLERY_TABLE);
            $result['entry_id'] = $this->db->insert_id();
        }else{
            $this->db->where('entry_id', $entry_id);
            $this->db->update(self::GALLERY_TABLE);
            $result['entry_id'] = $entry_id;
        }
         
        return array_merge($this->db->error(),$result);
    }

    public function get_entries_by_userid($user_id = null,$limit = 0,$start = 0){

        if(!is_null($user_id)){
            $this->db->select('*');
            $this->db->where('user_id', $user_id);
            $this->db->where('status',1);
            $this->db->from(self::GALLERY_TABLE . ' as gallery');
            $this->db->order_by('gallery.created_date', "desc");

            if($limit > 0){
                $this->db->limit($limit,$start);
            }
            return $this->db->get();
        }
    }

    public function get_entry_by_id($entry_id = null){

        if(!is_null($entry_id)){
            $this->db->select('*');
            $this->db->where('entry_id', $entry_id);
            $this->db->from(self::GALLERY_TABLE . ' as gallery');
            $this->db->join('tbl_user', 'tbl_user.user_id = gallery.user_id');
            $this->db->order_by('gallery.created_date', "desc");

            return $this->db->get();
        }
    }

    public function get_entries($limit = 0,$start = 0){

        $status = $this->session->userdata("filter_status");

        $this->db->select('*');
        $this->db->from(self::GALLERY_TABLE . ' as gallery');
        $this->db->join('tbl_user', 'tbl_user.user_id = gallery.user_id');

        if($status != 2 && isset($status)){
            $this->db->where('gallery.status',$status);
        }

        if($limit > 0){
            $this->db->limit($limit,$start);
        }
        $this->db->order_by('gallery.created_date', "desc");

        return $this->db->get();

    }

    /**
     * Return Active entries
     *
     * @param int $limit
     * @param int $start
     * @return mixed
     */
    public function get_active_entries($limit = 0,$start = 0){

        $this->db->select('*');
        $this->db->from(self::GALLERY_TABLE . ' as gallery');
        $this->db->join('tbl_user', 'tbl_user.user_id = gallery.user_id');
        $this->db->where('gallery.status',1);
        $this->db->order_by('gallery.created_date', "desc");

        if($limit > 0){
            $this->db->limit($limit,$start);
        }

        return $this->db->get();

    }

    /**
     * Query Result to CSV
     * @return mixed
     */
    public function get_entries_for_csv(){

        $status = $this->session->userdata("filter_status");
        $image_url = base_url('media/images/base/');
        $share_url = base_url('gallery/view/detail/');
        $this->db->select("tbl_user.fb_id as fbuid, tbl_user.first_name as first_name, tbl_user.last_name as  last_name,tbl_user.email,
                            tbl_user.address,tbl_user.region,tbl_user.dob as bday,tbl_user.mobile_number as mobile_no,tbl_user.product_purchased,tbl_user.agree_to_terms as agreetoterms,
                            gallery.entry_id as photoid, CONCAT('{$share_url}',gallery.entry_id) as share_url,
                            gallery.status as isvalid, gallery.created_date as date_added,
                            CONCAT('{$image_url}', gallery.image) as filename, gallery.filter_used");
        $this->db->from(self::GALLERY_TABLE . ' as gallery');
        $this->db->join('tbl_user', 'gallery.user_id = tbl_user.user_id');

        if($status != 2 && isset($status)){
            $this->db->where('gallery.status',$status);
        } 

        return $this->db->get();
    }


}