<?php


class Image_model extends CI_Model{

    const IMAGE_THUMBNAIL_PATH = 'media/images/thumbnail/';
    const IMAGE_BASE_PATH = 'media/images/base/';
    const IMAGE_FB_PATH = 'media/images/fb/';
    const IMAGE_DIR_400x400  = 'media/images/400x400/';


    var $image_name = '';
    var $image_path = '';
    var $image_type = '';

    /**
     * Image_model constructor.
     */
    public function __construct(){

        parent::__construct();

        $this->_init();
        $this->load->library('image_lib');

    }

    /**
     * Init
     */
    private function _init(){


        //create temporary directory
        if(!is_dir(DIR_TEMP)){
            mkdir(DIR_TEMP,0777,TRUE);
        }

        //create Thumb Image directory
        if(!is_dir(DIR_THUMB)){
            mkdir(DIR_THUMB,0777,TRUE);
        }
        //create Image directory
        if(!is_dir(DIR_IMAGE)){
            mkdir(DIR_IMAGE,0777,TRUE);
        }

        if(!is_dir(DIR_WATERMARK)){
            mkdir(DIR_WATERMARK,0777,TRUE);
        }

        if(!is_dir(DIR_400x400)){
            mkdir(DIR_400x400,0777,TRUE);
        }


    }

    public function save($image){

        $this->_resize($image,DIR_THUMB . $image->image_name,315,315);
        $this->_resize($image,DIR_IMAGE . $image->image_name);
        $this->_resize($image,DIR_400x400 . $image->image_name,400,400);

    }

    protected function _resize($image,$new_path,$width = null, $height = null){

        $config['image_library'] = 'gd2';
        $config['source_image'] =  $image->image_path;
        $config['new_image'] = $new_path;
        $config['maintain_ratio'] = TRUE;
        $config['width']  =  $width;
        $config['height'] =  $height;

        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();

        $this->_reize_for_fb_share(DIR_THUMB . $image->image_name,$image->image_name);
    }

    protected function _reize_for_fb_share($image_path,$image_name){

        $blank_image = DIR_WATERMARK . $image_name;

        $img = imagecreatetruecolor(600, 315);
        $bg = imagecolorallocate ( $img, 255, 255, 255 );
        imagefilledrectangle($img,0,0,600,315,$bg);
        imagejpeg($img,$blank_image,100);

        if($this->is_image_exist($blank_image)){

            $config['image_library'] = 'gd2'; //default value
            $config['source_image'] = $blank_image; //get original image
            $config['wm_type'] = 'overlay';
            $config['wm_overlay_path'] = $image_path;
            $config['wm_opacity'] = '100';
            $config['wm_vrt_alignment'] = 'middle';
            $config['wm_hor_alignment'] = 'center';
            $this->image_lib->initialize($config);
            if (!$this->image_lib->watermark()) {
                echo $this->image_lib->display_errors();
            }

            $this->image_lib->clear();
        }
    }

    /**
     *Create temporary image
     *
     * @param $base64_image
     * @return $this
     */
    public function temp_image($base64_image = null,$image_name = null,$image_type = 'jpeg'){

        if(!is_null($base64_image && !is_null($image_name))){
            $data = str_replace('data:image/gif;base64,', '', $base64_image);
            $data = str_replace(' ', '+', $data);
            $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64_image));

            $this->image_name = $image_name . ".{$image_type}";
            $this->image_path = DIR_TEMP . $this->image_name;

            file_put_contents($this->image_path,$image);

            return $this;
        }

        return false;

    }

    public function is_image_exist($image_path){
        if (file_exists($image_path)){
            return TRUE;
        }
        return FALSE;
    }

    public function get_400x400_path($image_name){
        return base_url(self::IMAGE_DIR_400x400 . $image_name);
    }

    public function get_base_path($image_name){
        return base_url(self::IMAGE_BASE_PATH . $image_name);
    }

    public function get_thumbnail_path($image_name){
        return base_url(self::IMAGE_THUMBNAIL_PATH . $image_name);
    }

    public function get_resized_path($image_name){
        return base_url(self::IMAGE_FB_PATH . $image_name);
    }

}
