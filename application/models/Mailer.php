<?php

class Mailer extends PHPMailer{
 
    /**
     * @var
     */
    var $_mailer;

    /**
     * Mailer constructor.
     */
    public function __construct(){

        parent::__construct();

        $this->_init();
    }

    /**
     * Initial Mailer Class
     */
    public function _init(){


        $this->isSMTP();                                      // Set mailer to use SMTP
        $this->Host =  config_item('mailer_host');                     // Specify main and backup SMTP servers
        $this->SMTPAuth = config_item('mailer_smtpauth');                               // Enable SMTP authentication
        $this->Username = config_item('mailer_username');                       // SMTP username
        $this->Password = config_item('mailer_password');                           // SMTP password
        $this->SMTPSecure = config_item('mailer_smtpsecure');                            // Enable TLS encryption, `ssl` also accepted
        $this->Port = config_item('mailer_port');

        return $this;
    }

}