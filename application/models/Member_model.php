<?php

class Member_model extends CI_Model {

       public function get_member_by_fb($fb_id) {
           $query = $this->db->query("SELECT * FROM tbl_user WHERE fb_id='$fb_id' LIMIT 1");
           return $query->row();
       }
       
       public function save_member($member) {           
           if( empty( $member['email'] ) or !filter_var($member['email'], FILTER_VALIDATE_EMAIL) ) return redirect('home/index/result/error');
           $this->db->insert('tbl_user', $member); 
           return $this->db->insert_id();
       }

}