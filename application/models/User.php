<?php

Class User extends CI_Model{

    const USER_TABLE = 'tbl_user';
    const USER_TYPE_DEFAULT = 1;
    const USER_TYPE_ADMIN = 2;

    protected $_is_logged_in;
    protected $_result;

    /**
     * User constructor.
     */
    public function __construct(){
        parent::__construct();

        $this->_is_logged_in = FALSE;
    }

    /**
     * @return int
     */
    public function get_admin_user_type(){
        return self::USER_TYPE_ADMIN;
    }

    /**
     * @return int
     */
    public function get_default_user_type(){
        return self::USER_TYPE_DEFAULT;
    }

    /**
     * Validate User Information
     *
     * @param $username
     * @param $password
     * @return $this
     */
    public function authenticate($username,$password){

        $this->db->select('*');
        $this->db->from(self::USER_TABLE);
        $this->db->where('username', $username);
        $this->db->where('password', MD5($password));
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() == 1){
            $this->_is_logged_in = TRUE;
            $this->_result = $query->row();
        }
        else{
            $this->_is_logged_in = FALSE;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function get_userdata(){
        return $this->_result;
    }

    /**
     * @return bool
     */
    public function is_login(){
        $this->session->set_userdata('logged_in', TRUE);
        return $this->_is_logged_in;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function load_by_id($id){

        $this->db->select('*');
        $this->db->from(self::USER_TABLE);
        $this->db->where('user_id', $id);
        $this->db->limit(1);

        return $this->db->get()->row();
    }

    /**
     * @param $email
     * @return mixed
     */
    public function load_by_email($email){

        $this->db->select('*');
        $this->db->from(self::USER_TABLE);
        $this->db->where('email', $email);
        $this->db->limit(1);

        return $this->db->get()->row();
    }

    public function load_admin_by_email($email){
        
        $this->db->select('*');
        $this->db->from(self::USER_TABLE);
        $this->db->where('email', $email);
        $this->db->where('usertype', self::USER_TYPE_ADMIN);
        $this->db->limit(1);

        return $this->db->get()->row();
    }

    /**
     * @param null $usertype
     * @return bool
     */
    public function user_collection($usertype = null){

        if(!is_null($usertype)){

            $this->db->select("*,CONCAT(first_name,' ',last_name ) as fullname",FALSE);
            $this->db->from(self::USER_TABLE);
            $this->db->where('usertype', $usertype);

            return $this->db->get();
        }

        return false;
    }

    /**
     * @param array $data
     * @param null $user_id
     * @return bool
     */
    public function update($data = array(),$user_id = null){

        if($user_id){
            $this->db->set($data);
            $this->db->where('user_id',$user_id);
            $this->db->update(self::USER_TABLE);

            return TRUE;
        }
        return FALSE;
    }
}