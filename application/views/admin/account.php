<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                My Account Form
            </header>
            <div class="panel-body">
                <form class="form-horizontal" action="<?php echo base_url('admin/account/update') ?>" method="post" role="form">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Username</label>
                        <div class="col-lg-10">
                            <input type="text" value="<?php echo $user->username ?>" name="username" class="form-control" id="username" placeholder="Username" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">First Name</label>
                        <div class="col-lg-10">
                            <input type="text" value="<?php echo $user->first_name ?>" name="first_name" class="form-control" id="first_name" placeholder="First Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Last Name</label>
                        <div class="col-lg-10">
                            <input type="text" value="<?php echo $user->last_name ?>" name="last_name" class="form-control" id="last_name" placeholder="Last Name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-10">
                            <input type="email" name="email"  value="<?php echo $user->email ?>" class="form-control" id="inputEmail1" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword1" class="col-lg-2 control-label">Password</label>
                        <div class="col-lg-10">
                            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                            <p class="help-block">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="chage_password" name="change_password">Change Password
                                </label>
                            </div>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>