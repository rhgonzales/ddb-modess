<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo isset($title) ? $title : ""; ?></title>
    <?php $this->load->view('admin/page/header') ?>
</head>
<body>
<form class="login-form" method="post" action="<?php echo base_url('admin/authentication/forgotpassword_submit'); ?>">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
    <div class="login-wrap">
        <h3>Forgot Password</h3>
        <div class="input-group " style="width: 100%">
            <label>Email</label>
            <input type="text" name="email" class="form-control" placeholder="" autofocus>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button><br>
        <a class="" href="<?php echo base_url('admin') ?>">&larr; Back To Login</a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?php $this->load->view('admin/page/notification'); ?>
        </div>
    </div>
</form>
<?php $this->load->view('admin/page/footer') ?>
</body>
</html>
