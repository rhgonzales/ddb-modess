<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="col-sm-2"><?php echo $this->pagination->create_links(); ?></div>
                <div class="col-sm-4 pull-left">
                    <div class="form-horizontal ">
                        <label class="control-label col-lg-6" for="selectStatus">Filter By Status:</label>
                        <div class="col-lg-4">
                            <select id="selectStatus"  data-request-url="<?php echo base_url('admin/gallery/filter_status') ?>" data-base-url="<?php echo base_url('admin/gallery/') ?>" class="form-control input-sm ">
                                <option value="2"  <?php echo ($filter_status == 2) ? "selected" : ''?>>Select All</option>
                                <option value="1" <?php echo ($filter_status == 1) ? "selected" : ''?>>Active</option>
                                <option value="0" <?php echo ($filter_status == 0) ? "selected" : ''?>>Not Active</option>
                            </select>
                        </div>
                    </div>
                 </div>
                <div class="col-sm-4 form-horizontal ">
                    <label class="control-label col-lg-6" for="exportTo">Export To: </label>
                    <div class="col-lg-4">
                        <select id="exportTo" data-base-url="<?php echo base_url('admin/gallery/') ?>" class="form-control input-sm ">
                            <option value="" >Select here</option>
                            <option value="export_to_csv">CSV File</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2 pull-right form-horizontal ">
                    <label class="control-label col-lg-12" for="selectStatus"><strong>Total Entries: (<?php echo $total_entries ?>)</strong></label>
                </div>
        </section>
    </div>
</div>
<Style>

</Style>
<?php if($entries->num_rows()): ?>
<?php $display_label = array(0 => 'Show',1 => 'Hide') ?>
<div class="row entries" id="gallery-container" data-csrf-token="<?php echo $this->security->get_csrf_hash(); ?>">
    <?php foreach ($entries->result() as $item): ?>
        <?php
            $image_thumb_url = base_url('media/images/thumbnail/' . $item->image);
            $image_url = base_url('media/images/base/' . $item->image);

            $item->formatDob = date('F d, Y',strtotime($item->dob));
        ?>
        <div class="col-lg-3 list">
            <section class="panel ">
                <div class="panel-body <?php echo "list-item-{$item->entry_id}" ?>"  data-details="<?php echo htmlentities(json_encode($item)) ?>" data-image-url="<?php echo $image_url ?>" data-option-status="<?php echo $item->status ?>" data-entry-id="<?php echo $item->entry_id ?>" data-action-url="<?php echo base_url('admin/gallery/update_status') ?>">
                    <div class="image-handle btn-modal"  data-entry-id="<?php echo $item->entry_id ?>"><img width="236" src="<?php echo $image_thumb_url; ?>" alt="<?php echo  $item->image ?>"></div>
                    <div class="info-handle">
                        <div class="photo-id"><strong>Photo ID:</strong><span><?php echo $item->entry_id ?></span></div>
                        <div class="txt"><strong>First Name:</strong><span><?php echo $item->first_name ?></span></div>
                        <div class="txt"><strong>Last Name:</strong><span><?php echo  $item->last_name ?></span></div>
                        <div class="txt"><strong>Email:</strong><span><?php echo $item->email ?></span></div>
                        <div class="txt address" title="<?php echo $item->address ?>"><strong>Complete Address:</strong><span><?php echo strlen($item->address) > 15 ? substr($item->address,0,15)."..." : $item->address; ?></span></div>
                        <div class="txt"><strong>Regional Address:</strong><span><?php echo $item->region ?></span></div>
                        <div class="txt"><strong>Date of Birth:</strong><span><?php echo $item->formatDob ?></span></div>
                        <div class="txt"><strong>Mobile Number:</strong><span><?php echo $item->mobile_number ?></span></strong></div>
                    </div>
                    <div class="button-handle">
                        <div class="btn-group btn-group-justified option-status" >
                            <a class="btn btn-info btn-modal" href="" data-entry-id="<?php echo $item->entry_id ?>"><strong>View</strong></a>
                            <a class="btn btn-info toggle" href="" data-entry-id="<?php echo $item->entry_id ?>"><strong><?php echo $display_label[$item->status] ?></strong></a>
                        </div>
                    </div> 
                </div>
            </section>
        </div>
    <?php endforeach; ?>
</div>
<?php else: ?>
    <div class="alert alert-warning fade in">
        <p>No record available.</p>
    </div>
<?php endif; ?>


<!-- Modal -->
<div class="modal fade" id="galleryModal" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><strong>Photo ID: (<span class="id"></span>) -- Status: <span class="status"></span></strong></h4>
            </div>
            <div class="modal-body">
                <div class="image-wrapper text-center" ></div>
                <div class="info-handle">
                    <div class="photo-id photo_id"><strong>Photo ID:</strong><span></span></div>
                    <div class="txt first_name"><strong>First Name:</strong><span></span></div>
                    <div class="txt last_name"><strong>Last Name:</strong><span></span></div>
                    <div class="txt email"><strong>Email:</strong><span></span></div>
                    <div class="txt complete_address"><strong>Complete Address:</strong><span></span></div>
                    <div class="txt regional_address"><strong>Regional Address:</strong><span></span></div>
                    <div class="txt dob"><strong>Date of Birth:</strong><span></span></div>
                    <div class="txt mobile_number"><strong>Mobile Number:</strong><span></span></strong></div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn  btn-info toggle" data-action-url="<?php echo base_url('admin/gallery/update_status') ?> type="button"></button>
                <button data-dismiss="modal" class="btn btn-default" type="button"><strong>Close</strong></button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
