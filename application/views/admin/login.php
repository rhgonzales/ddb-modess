<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo isset($title) ? $title : ""; ?></title>
    <?php $this->load->view('admin/page/header') ?>
</head>
<body>
<form class="login-form" method="post" action="<?php echo base_url('admin/authentication/login'); ?>">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
    <div class="login-wrap">
        <p class="login-img"><i class="icon_lock_alt"></i></p>
        <div class="input-group">
            <span class="input-group-addon"><i class="icon_profile"></i></span>
            <input type="text" name="username" class="form-control" placeholder="Username" autofocus required>
        </div>
        <div class="input-group">
            <span class="input-group-addon"><i class="icon_key_alt"></i></span>
            <input type="password" name="password" class="form-control" placeholder="Password" required>
        </div>
        <label class="checkbox">
            <span class="pull-right"> <a href="<?php echo base_url('admin/authentication/forgotpassword') ?>"> Forgot Password?</a></span>
        </label>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?php $this->load->view('admin/page/notification'); ?>
        </div>
    </div>
</form>
<?php $this->load->view('admin/page/footer') ?>
</body>
</html>
