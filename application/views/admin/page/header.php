<!-- Header Section -->
<!-- Bootstrap CSS -->
<link href="<?php echo base_url('assets/theme/admin/apd/css/bootstrap.min.css') ?>" rel="stylesheet">
<!-- bootstrap theme -->
<link href="<?php echo base_url('assets/theme/admin/apd/css/bootstrap-theme.css') ?>" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="<?php echo base_url('assets/theme/admin/apd/css/elegant-icons-style.css') ?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/theme/admin/apd/css/font-awesome.css') ?>" rel="stylesheet" />
<!-- Custom styles -->
<link href="<?php echo base_url('assets/theme/admin/apd/css/style.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/theme/admin/apd/css/style-responsive.css') ?>" rel="stylesheet" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
<script src="<?php echo base_url('assets/theme/admin/apd/js/html5shiv.js') ?>"></script>
<script src=" <?php echo base_url('assets/theme/admin/apd/js/respond.min.js') ?>"></script>
<![endif]-->

<!-- javascripts -->
<script src=" <?php echo base_url('assets/theme/admin/apd/js/jquery.js')?>"></script>
<script src=" <?php echo base_url('assets/theme/admin/apd/js/jquery-ui-1.10.4.min.js')?>"></script>
<script src=" <?php echo base_url('assets/theme/admin/apd/js/jquery-1.8.3.min.js')?>"></script>
<script type="text/javascript" src=" <?php echo base_url('assets/theme/admin/apd/js/jquery-ui-1.9.2.custom.min.js')?>"></script>
<!-- bootstrap -->
<script src=" <?php echo base_url('assets/theme/admin/apd/js/bootstrap.min.js')?>"></script>
<!-- nice scroll -->
<script src=" <?php echo base_url('assets/theme/admin/apd/js/jquery.scrollTo.min.js')?>"></script>
<script src=" <?php echo base_url('assets/theme/admin/apd/js/jquery.nicescroll.js')?>" type="text/javascript"></script>
<!-- custom select -->
<script src=" <?php echo base_url('assets/theme/admin/apd/js/jquery.customSelect.min.js')?>" ></script>


<!--custome script for all page-->
<script src=" <?php echo base_url('assets/theme/admin/apd/js/scripts.js')?>"></script>
<script src=" <?php echo base_url('assets/theme/admin/js/custom.js')?>"></script>
