<?php
    $alert_success = $this->session->flashdata('alert_success');
    $alert_danger = $this->session->flashdata('alert_danger');
    $alert_info = $this->session->flashdata('alert_info');
    $alert_warning = $this->session->flashdata('alert_warning');
?>

<?php if(isset($alert_success)): ?>
<div class="alert alert-success fade in">
    <button data-dismiss="alert" class="close close-sm" type="button">
        <i class="icon-remove"></i>
    </button>
    <?php echo $alert_success; ?>
</div>
<?php endif ?>

<?php if(isset($alert_danger)): ?>
<div class="alert alert-block alert-danger fade in">
    <button data-dismiss="alert" class="close close-sm" type="button">
        <i class="icon-remove"></i>
    </button>
    <?php echo $alert_danger; ?>
</div>
<?php endif ?>

<?php if(isset($alert_info)): ?>
<div class="alert alert-info fade in">
    <button data-dismiss="alert" class="close close-sm" type="button">
        <i class="icon-remove"></i>
    </button>
    <?php echo $alert_info; ?>
</div>
<?php endif ?>

<?php if(isset($alert_warning)): ?>
<div class="alert alert-warning fade in">
    <button data-dismiss="alert" class="close close-sm" type="button">
        <i class="icon-remove"></i>
    </button>
    <?php echo $alert_warning; ?>
</div>
<?php endif ?>