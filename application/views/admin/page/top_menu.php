<?php
 $user = $this->session->get_userdata();
?>
<ul class="nav pull-right top-menu">
    <!-- user login dropdown start-->
    <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="profile-ava">
                    <img alt="" src="<?php echo base_url('assets/theme/admin/apd/img/girl-512.png') ?>">
                </span>
            <span class="username"><?php echo $user['name'] ?></span>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu extended logout">
            <div class="log-arrow-up"></div>
            <li class="eborder-top">
                <a href="<?php echo base_url('admin/account'); ?>"><i class="icon_profile"></i> My Account</a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/gallery'); ?>"><i class="icon_image"></i>Gallery Entries</a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/authentication/logout'); ?>"><i class="icon_key_alt"></i> Log Out</a>
            </li>
        </ul>
    </li>
    <!-- user login dropdown end -->
</ul>