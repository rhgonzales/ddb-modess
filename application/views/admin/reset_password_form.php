<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo isset($title) ? $title : ""; ?></title>
    <?php $this->load->view('admin/page/header') ?>
</head>
<body>
<form class="login-form" method="post" action="<?php echo base_url('admin/authentication/resetpassword_submit'); ?>">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
    <div class="login-wrap">
        <h3>Reset Password</h3>
        <div class="input-group " style="width: 100%">
            <label>New Password</label>
            <input type="password" name="new_password" class="form-control" placeholder="" autofocus>
        </div>
        <div class="input-group " style="width: 100%">
            <label>Confirm Password</label>
            <input type="password" name="confirm_password" class="form-control" placeholder="" autofocus>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button><br>
        <a class="" href="<?php echo base_url('admin') ?>">&larr; Back To Login</a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?php $this->load->view('admin/page/notification'); ?>
        </div>
    </div>
    <input type="hidden" name="user_id" value="<?php echo $this->input->get('user_id'); ?>">
    <input type="hidden" name="email" value="<?php echo $this->input->get('email'); ?>">
    <input type="hidden" name="auth_key" value="<?php echo $this->input->get('auth_key'); ?>">
    <input type="hidden" name="current_url" value="<?php echo base_url(uri_string()) . '?'. $_SERVER['QUERY_STRING']; ?>">
</form>
<?php $this->load->view('admin/page/footer') ?>
</body>
</html>
