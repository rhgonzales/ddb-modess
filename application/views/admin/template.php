<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo isset($title) ? $title : ""; ?></title>
    <?php $this->load->view('admin/page/header') ?>
</head>
<body>
<section id="container" class="sidebar-closed">
    <header class="header dark-bg">
        <?php /*<div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
        </div>*/ ?>

        <!--logo start-->
        <a href="<?php echo base_url('admin/gallery') ?>" class="logo"><span class="lite">Administrator</span></a>
        <!--logo end-->

        <div class="top-nav notification-row">
            <?php $this->load->view('admin/page/top_menu'); ?>
        </div>
    </header>
    <!--header end-->

    <!--sidebar start-->
    <?php /*<aside>
        <div id="sidebar"  class="nav-collapse ">
            <?php $this->load->view('admin/page/sidebar_menu'); ?>
        </div>
    </aside> */ ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!--overview start-->
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?php echo isset($header_title) ? $header_title : ""; ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php $this->load->view('admin/page/notification'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php echo isset($content) ? $content : ""; ?>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->
</section>
    <?php $this->load->view('admin/page/footer') ?>
</body>
</html>
