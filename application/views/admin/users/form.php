<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Profile Information
            </header>
            <div class="panel-body">
                <form class="form-horizontal" action="<?php echo base_url('admin/users/update') ?>" method="post" role="form">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">First Name</label>
                        <div class="col-lg-10">
                            <input type="text" value="<?php echo $user->first_name ?>" name="first_name" class="form-control" id="first_name" placeholder="First Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Last Name</label>
                        <div class="col-lg-10">
                            <input type="text" value="<?php echo $user->last_name ?>" name="last_name" class="form-control" id="last_name" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Gender</label>
                        <div class="col-lg-10">
                           <select class="form-control" name="gender">
                               <option value="" disabled>Please Select</option>
                               <option value="0" <?php echo ($user->gender == 0) ? "Selected" : ""; ?>>Male</option>
                               <option value="1" <?php echo ($user->gender == 1) ? "Selected" : ""; ?>>Female</option>
                           </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Birthday</label>
                        <div class="col-lg-10">
                            <input type="text" value="<?php echo $user->dob ?>" name="dob" class="form-control" id="dob" placeholder="Birthday">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-10">
                            <input type="email" name="email" value="<?php echo $user->email ?>" class="form-control" id="inputEmail1" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Address</label>
                        <div class="col-lg-10">
                            <input type="text" name="address" value="<?php echo $user->address ?>" class="form-control" id="inputAddress" placeholder="Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Is active?</label>
                        <div class="col-lg-10">
                            <select class="form-control">.
                                <option value="" disabled>Please Select</option>
                                <option value="0" <?php echo ($user->is_active == 0) ? "Selected" : ""; ?>>No</option>
                                <option value="1" <?php echo ($user->is_active == 1) ? "Selected" : ""; ?>>Yes</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword1" class="col-lg-2 control-label">Password</label>
                        <div class="col-lg-10">
                            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                            <p class="help-block">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="change_password">Change Password
                                </label>
                            </div>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <input type="hidden" value="<?php echo $user->user_id ?>" name="user_id">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>