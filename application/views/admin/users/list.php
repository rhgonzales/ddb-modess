<?php if($user_list): ?>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <table class="table table-striped table-advance table-hover">
                    <tbody>
                    <tr>
                        <th><i class="icon_profile"></i> Full Name</th>
                        <th><i class="icon_calendar"></i> DOB</th>
                        <th><i class="icon_mail_alt"></i> Email</th>
                        <th><i class="icon_pin_alt"></i> Address</th>
                        <th><i class="icon_cogs"></i> Action</th>
                    </tr>
                    <?php foreach($user_list->result() as $user): ?>
                    <tr>
                        <td><?php echo $user->fullname ?></td>
                        <td><?php echo $user->dob ?></td>
                        <td><?php echo $user->email ?></td>
                        <td><?php echo $user->address ?></td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-primary" href="<?php echo base_url('admin/users/view/'.$user->user_id) ?>"> View</a>
                                <a class="btn btn-danger" href="#"> Remove</a>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </section>
        </div>
    </div>
<?php else: ?>
    <div class="alert alert-warning fade in">
        <p>No record available.</p>
    </div>
<?php endif; ?>
