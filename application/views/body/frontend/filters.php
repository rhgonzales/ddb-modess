<?php 
	$filters = config_item('ddb_filters');
?>

	<section class="page-background-container filter">
		<div class="page-background-wrapper"></div>
			<div class="content-wrapper filters-wrapper">
				<h1>#COURAGEUNLOCKED FILTERS</h1>
				<div class="intro-banner-block text-center">
					<p>Choose a filter to view and find out more about it!</p>
				</div>
				<div class="content-section  filters-container">
					<ul class="filter-list clearfix">
						<?php
							foreach ($filters as $filter) {
								echo '<li>
										<a id="image-filter-'.$filter['id'].'" class="trackme" data-open="filter-'.$filter['id'].'">
											<img src="'.base_url().$filter['frame'].'">
										</a>
										<p>'.$filter['name'].'</p>
									</li>';
							}
						?>
					</ul>
					<p class="text-center">
						<a id="link-home" href="<?php echo base_url() ?>" class="button block blue trackme">home</a>
					</p>
				</div>
				
			</div>
	</section>
	

	<?php
		foreach ($filters as $filter) {
			$currentID = $filter['id'];
			$filtersCount = count($filters);
			
			$next = 'filter-'.($currentID + 1);
			$back = 'filter-'.($currentID - 1);

			if ($currentID == $filtersCount) {
				$next = 'filter-1';
			}

			if ($currentID == 1) {
				$back = 'filter-'.$filtersCount;
			}
			
			echo '
				<div class="reveal filter-reveal-container small" id="filter-'.$filter['id'].'" data-reveal>       
					<div class="row">
						<div class="small-12 filter-reveal-img">
							<img src="'. base_url('/'.$filter['sample']).'">
						</div>
						<div class="small-12 filter-reveal-desc">
							<h3>'.$filter['name'].'</h3>
							<p>'.$filter['description'].'</p>
						</div>
						<div class="small-12 filter-reveal-button">
							<a id="link-join-now" href="'.$selfie_link.'" class="trackme button full pink">Take Selfie</a>
							<a data-open="'.$back.'" class="trackme button half back trackme">Back</a>
							<a data-open="'.$next.'" class="trackme button half next trackme">Next</a>
						</div>
					</div>
					<button class="close-button trackme" data-close aria-label="Close modal" type="button">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			';
		}
	?>
