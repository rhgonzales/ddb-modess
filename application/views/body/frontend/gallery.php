<section class="page-background-container gallery">
	<div class="page-background-wrapper"></div>
	<div class="content-wrapper text-center tab-1" id="tab-container">
		<h1>Gallery</h1>
		<p class="sub-title">Browse through photos that inspire you to unlock courage.</p>

		<ul class="tabs gallery-tabs">
			<?php if(!isset($my_entries)){?>
				<li class="tab-title tabs-title active trackme" data-tab="tab-1" style="width: 100%">All entries</li>
			<?php } ?> 
			<?php if(isset($my_entries)): ?>
				<li class="tab-title tabs-title active trackme" data-tab="tab-1">All entries</li>
		  		<li class="tab-title tabs-title trackme" data-tab="tab-2">My Entries</li>
		  	<?php endif; ?>
		</ul>

		<div class="tabs-content">
			<!-- start of all entries -->
		  <div class="tab-content tab-1 active" data-url="<?php echo base_url('gallery/all_entries') ?>">
		    <ul class="gallery-thumbs-list clearfix"></ul>
			  <div class="row text-center">
				  <a href="#" id="load-more" class="button full load load-more trackme">Load more</a>
			  </div>
		  </div>
		  	<!-- end all entries -->

			<!-- start of my entries -->
			<?php if(isset($my_entries)): ?>
		  	<div class="tab-content tab-2" data-url="<?php echo base_url('gallery/my_entries') ?>">
				<ul class="gallery-thumbs-list clearfix"></ul>
			  <div class="row text-center">
				  <a href="#" id="load-more" class="button full load load-more trackme">Load more</a>
			  </div>
		 	 </div>
		 	 <?php endif; ?>
			<!-- end of my entries -->
		</div>

		<!-- compoment navigator links -->
		<div class="helper-menu-container">
			<ul>
				<li>
					<a href="<?php echo $home_link ?>" class="trackme">
						<span class="img-cont"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/helper-icons-menu-house_06.png')?>"></span>
						Return <br/>Home
					</a>
				</li>
				
				<?php if(isset($my_entries)): ?>
				<li class="tab-title" data-tab="tab-2">
					<a href="#" class="trackme">
						<span class="img-cont"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/helper-icon-menu-gallery_06.png')?>"></span>
						View my<br/> gallery
					</a>
				</li>
				<?php endif; ?>

				<li class="tab-title active" data-tab="tab-1">
					<a href="#" class="trackme">
						<span class="img-cont"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/gallery-icon_03.png')?>"></span>
						View all<br/> entries
					</a>
				</li>
				<li>
					<a href="<?php echo $photo_link ?>" class="trackme">
						<span class="img-cont"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/helper-icon-menu-filter_06.png')?>"></span>
						Create new<br/>entry
					</a>
				</li>
			</ul>
		</div>
	</div>
</section>
