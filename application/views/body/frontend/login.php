<?php echo isset($home_nav) ? $home_nav : "" ?> 
<section class="banner-container">
    <div class="wrapper-container full-height clearfix">
        <div class="heading-contianer">
            <img src="<?php echo base_url('assets/theme/FE/dist/assets/img/desktop-heading_03.png')?>">
            <a id="join-now" href="<?php echo $login_url; ?>" class="hot-button trackme"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/join-button_03.png')?>"></a>
        </div>      
        <ul class="brands-list">
            <li><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brqand-modess_07.png')?>"></li>
            <li><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brand-clean&clear_06.png')?>"></li>
            <li><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brand-carefree_07.png')?>"></li>
        </ul>
        <a href="#slider-container" class="anchor-arrow trackme"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/anchor-arrow_03.png')?>"/></a>
    </div>
</section>

<section class="slider-container" id="slider-container">
    <div class="wrapper-container">
        <div class="slider-wrapper clearfix">
            <div class="slider-image-panel column medium-6">
                <div class="homepage-slider">            
                    <div class="slider-item">
                        <a id="link-participating-products" href="<?php echo base_url('participating-products'); ?>" class="trackme"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/step-1-slider_03.png')?>"></a>
                    </div>
                    <div class="slider-item">
                        <a id="link-filters" href="<?php echo base_url('filters'); ?>" class="trackme"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/step-2-slider_03.png')?>"></a>
                    </div>
                    <div class="slider-item">
                        <img src="<?php echo base_url('assets/theme/FE/dist/assets/img/step-3-slider_03.png')?>">
                    </div>
                </div>  
                <button type="button" class="prev-arrow trackme" aria-label="next"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/prev-arrow_07.png')?>"></button>
                <button type="button" class="next-arrow trackme" aria-label="next"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/next-arrow_06.png')?>"></button>         
            </div>
            <div class="slider-content-panel column medium-6">
                <a id="join-now" href="<?php echo $login_url; ?>" class="join-button trackme">Join now</a>
                <ul class="rounded-link-list">
                    <li> <a id="link-mechanics" href="<?php echo base_url('mechanics') ?>" class="trackme"> Read Mechanics</a></li>
                    <li> <a id="link-filters" href="<?php echo base_url('filters'); ?>" class="trackme"> View <br/>Filters</a></li>
                    <li> <a id="link-gallery" href="<?php echo base_url('gallery'); ?>" class="trackme"> View <br/>Gallery</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>