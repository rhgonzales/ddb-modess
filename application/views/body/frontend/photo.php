<!-- <div class="result-photo-bypass">
	<div id="result1"></div>
	<div id="result2"></div>
	<div id="result3"></div>
	<div id="result4"></div>
</div> -->
<section class="photo-editor mobile page-take-photo" id="photo-editor">
	<div class="steps">
		<div class="pagination-wizard">
			<span class="wizard-indicator" arial-hidden="true"></span>
			<span class="wizard-indicator" arial-hidden="true"></span>
			<span class="wizard-indicator" arial-hidden="true"></span>
		</div>
	</div>
    <div class="editor">
    	<div class="menu">
    		<div class="line-bar"></div>
	    	<div class="wrapper-container">
		    	<ul>
		    		<li class="menu-take-photo active show" data-menu="take-photo">
		    			<span class="label-mobile">UPLOAD SELFIE</span>
		    			<span class="label-desktop">UPLOAD SELFIE</span>
		    		</li>
		    		<li class="menu-select-frames show" data-menu="select-frames">CHOOSE FILTER</li>
		    		<li class="menu-edit-photo" data-menu="edit-photo">
						<span class="label-mobile">EDIT PHOTO</span>
						<span class="label-desktop">EDIT SELFIE</span>
		    		</li>
		    		<li class="menu-submit" data-menu="submit">SUBMIT</li>
		    		<li class="menu-confirmation" data-menu="submit">CONFIRMATION</li>
		    	</ul>
		    </div>
		</div>
		
		<div class="row editor-content">
			<div class="modal-overlay disabled"></div>
			<div class="modal-overlay transparent"></div>
			<div class="modal-overlay" id="modal-overlay">
				<div class="mesages"></div>	
			</div>
			<div class="modal-overlay loading">
				<div class="mesages"><i class="fa fa-refresh fa-spin fa-2x fa-fw loader-icon"></i></div>	
			</div>
			<div class="modal-overlay landscape" id="modal-overlay">
				<div class="mesages">Sorry, the application work only in portrait mode.</br>Please rotate your device.</div>	
			</div>

			<div class="column large-6">
		    	<div class="canvas" id="canvas">
		    		<div class="video-wrapper">
		    			<video class="video" id="video" autoplay></video>
		    			<button class="capture-btn trackme" id="webcam-capture-btn">
		    				<i class="fa fa-camera" aria-hidden="true"></i>
		    			</button>
		    			<button class="close-btn trackme" id="webcam-close-btn">
		    				<i class="fa fa-times"></i>
		    			</button>
		    		</div>
		    		<div class="inside-area video-error">
		    			<div>Sorry, we can't detect your webcam device.</div>
		    		</div>
		            <div class="inside-area">
		                <div>Drag your photo here or click to add.</div>
		            </div>
		            <div id="preloader" class="inside-area preloader">
		                <i class="fa fa-spin fa-refresh fa-2x fa-fw"></i>
		            </div>
		    	</div>
		    	
		    </div>
			<div class="column large-6">
				<input type="hidden" id="config-url" value="<?php echo base_url('photo/capture') ?>">
	            <input type="hidden" id="config-token" value="<?php echo $this->security->get_csrf_hash()?>">
		    	<div class="tools" id="tools">
		            <div class="tool tool-take-photo show">
		                <div class="option option-webcam">
		                	<div class="tool-btn tool-btn-webcam trackme">
								<span class="icon icon-webcam"></span>
								<span class="tool-label">USE WEBCAM</span>
		                	</div>
		                </div>
		                <div class="option">
		                	<input type="file" class="fileInput" name="filesToUpload[]" id="filesToUpload" accept="image/jpeg, image/png" />
    	                	<div class="tool-btn tool-photo-gallery trackme">
    							<span class="icon icon-photos"></span> 
    							<span class="tool-label">FROM PHOTO GALLERY</span>
    	                	</div>

		                	<div class="tool-btn tool-btn-camera trackme">
								<span class="icon icon-camera"></span>
								<span class="tool-label">INSERT SELFIE</span>
		                	</div>

		                	<div class="file-browse trackme">
		                	    <div class="label">
		                	    	<span>BROWSE</span>
		                	    </div>
		                	    <input type="text" class="input" id="fileInputText" disabled>
		                	</div>
		                </div>
		            </div>
					
					<div class="tool tool-select-frames">
						<div class="frame-slider">
			                <ul id="frames"></ul>
			                <div id="frames-slider-next-arrow" class="arrow next-arrow trackme">
			                	<span class="icon icon-arrow-left"></span>
			                </div>
			                <div id="frames-slider-prev-arrow" class="arrow prev-arrow trackme">
			                	<span class="icon icon-arrow-left"></span>
			                </div>
			            </div>
		            </div>

		            <div class="tool tool-edit-photo" id="tool-edit-photo">
		                <ul class="tool-btns">
		                    <li class="tool-btn tool-btn-resize active trackme" data-subtool="sub-tool-resize">
		                        <span class="icon icon-resize"></span> RESIZE
		                    </li>
		                    <li class="tool-btn tool-btn-rotate trackme" data-subtool="sub-tool-rotate">
		                        <span class="icon icon-rotate"></span> ROTATE
		                    </li>
		                    <li class="tool-btn tool-btn-reset trackme" data-subtool="sub-tool-reset">
		                        <span class="icon icon-reset"></span> RESET
		                    </li>
		                </ul>

		                <div class="sub-tools">
		                    <div class="sub-tool sub-tool-resize show">
		                        <div class="sub-tool-name"><span class="icon icon-resize trackme"></span> Resize</div>

		                        <div class="btns">
		                            <div id="zoom-out-image" class="btn trackme">
		                                <div>
		                                    <i class="fa fa-minus"></i>
		                                </div>
		                            </div>
		                            <!-- <li>
		                            	<div id="resize-slider" class="slider" data-slider data-initial-start="0" data-end="12" data-step="2">
		                            	    <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
		                            	    <span class="slider-fill" data-slider-fill></span>
		                            	</div>
		                            </li> -->
		                            <div id="zoom-in-image" class="btn trackme">
		                                <div>
		                                    <i class="fa fa-plus"></i>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="sub-tool sub-tool-rotate">
		                    	<div class="sub-tool-name"><span class="icon icon-rotate trackme"></span> Rotate</div>
		                       	
		                       	<div class="btns">
	                                <div data-rotate="counter clockwise" class="btn rotate-btn trackme">
	                                    <i class="fa fa-undo" aria-hidden="true"></i> 
	                                </div>
		                            <!-- <li>
		                            	<div id="rotate-slider" class="slider" data-slider data-initial-start="0" data-end="270" data-step="90">
				                            <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
				                            <span class="slider-fill" data-slider-fill></span>
				                        </div>
		                            </li> -->
	                                <div data-rotate="clockwise" class="btn rotate-btn trackme">
	                                    <i class="fa fa-repeat" aria-hidden="true"></i>
	                                </div>
	                            </div>
		                    </div>
		                    <div class="sub-tool sub-tool-reset">
		                    	<div class="sub-tool-name"><span class="icon icon-reset trackme"></span> Reset</div>
		                        <button type="submit" class="button reset-image-btn trackme">Reset</button>
		                    </div>
		                </div> <!-- /.sub-tools -->
		            </div> <!-- /.edit-photo --> 

		            <div class="tool tool-submit">
						<div class="option">
			                <p>Ready to submit </br> your finished entry?</p>

			                <button type="submit" id="submit-photo" class="button trackme">SUBMIT PHOTO</button>
			                
			                <button type="submit" class="pagination-btn button button-back trackme" data-paginate="back">BACK</button>
			            </div>
		            </div>

		            <ul class="pagination" id="pagination">
		            	<li class="disabled pagination-btn trackme" data-paginate="back" id="paginate-back"><i class="fa fa-caret-left"></i> BACK</li>
		            	<li class="disabled pagination-btn trackme" data-paginate="next" id="paginate-next">NEXT <i class="fa fa-caret-right"></i></li>
		            </ul>
		    	</div>  <!-- /.tools -->
		    </div>
	    </div> <!-- / .row . editor-content --> 

	    <div class="row success-content">
	    	<div class="column large-6">
	    		<div id="result-photo" class="result-photo">
	    		</div>
	    	</div>
	    	<div class="column large-6">
	    		<div class="inner-content">
		    		<h3>YOU DID IT!</h3>
		    		<p>Thank you for your photo entry. </br> You'll receive a confirmation email from us shortly.</p>

		    		<!-- <a href="#" target="_blank" class="button" id="share-facebook-btn"><i class="fa fa-facebook"></i> SHARE THIS PHOTO</a>
		    		<br>
		    		<a href="#" target="_blank" class="button" id="share-twitter-btn"><i class="fa fa-twitter"></i> SHARE THIS PHOTO</a> -->
		    		<a href="#" class="button trackme" id="share-btn">SHARE THIS PHOTO</a>
		    		<br>
		    		<a href="<?php echo base_url() ?>" class="button trackme">HOME</a>
		    	</div>
	    	</div>
	    </div> <!-- / .row . success-content --> 
    </div>

    <div id="arrow-down" class="arrow-down-indicator trackme">
    	<img src="<?php echo base_url('/assets/theme/FE/dist/assets/img/anchor-arrow_04.png') ?>">
    </div>

</section>



<?php 
	$filters = config_item('ddb_filters');
	$data = [];
	foreach ($filters as $filter) {
        $data[] = [
            'id' => $filter['id'],
            'name' => $filter['name'],
            'img' => base_url('/'.$filter['frame'])
        ];
	}
?>

<script id="frames-json" type="text/x-json"><?php echo json_encode( $data ); ?></script>
