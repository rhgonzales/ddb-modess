<section class="page-background-container registration">
    <div class="page-background-wrapper">   </div>
    <div class="content-wrapper">
        <h1>Let's get started</h1>
        <p class="sub-title">Type in your details 'cause we want to know a little bit more about you</p>

        <div class="pagination-wizard">
            <span class="wizard-indicator active" arial-hidden="true"></span>
            <span class="wizard-indicator" arial-hidden="true"></span>
            <span class="wizard-indicator" arial-hidden="true"></span>
        </div>
        <div class="banner-block">
            <p>Sign up</p>
        </div>
        <form class="registration-form" method="post" action="<?php echo $action ?>">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name()?>" value="<?php echo $this->security->get_csrf_hash();?>" />
            <input type="hidden" name="refer" value="<?php echo isset($_GET['refer'])? $_GET['refer']:''; ?>">
			<i style="margin-left:50px">Please complete all fields</i>
            <div class="error-container-message">
                <h2>UH oh!</h2>
                <p>There seems to be an error in one of the fields. Please check and try again!</p>
            </div>
        <fieldset>

                <div class="row">
                    <div class="column medium-6 noPad">
                        <label>first name</label>
                        <input type="text" id="first_name" name="first_name"  value="<?php echo display_field($user_profile, 'first_name') ?>" data-error="This field is required" data-label="name" data-required/>
                    </div>

                    <div class="column medium-6 noPad">
                        <label>Last name</label>
                        <input type="text" id="last_name" name="last_name" value="<?php echo display_field($user_profile, 'last_name') ?>" data-error="This field is required" data-label="name" data-required/>
                    </div>
                </div>

                <div class="row">
                    <label>email address</label>
                    <?php $_email = display_field($user_profile, 'email'); ?>
                    <input type="email" id="email_address" <?php echo ( $_email != "" ) ? "readonly" : ""; ?> name="email" value="<?php echo $_email ?>" data-error="This field is required" data-label="email" data-required/>
                </div>
                <div class="row">
                    <label>Complete address</label>
                    <input type="text" name="address" data-error="This field is required" data-label="address" data-required/>
                </div>
                <div class="row relative">
                    <label>Regional Address</label>
                    <select data-error="This field is required" name="region" data-required>
                        <option value="" readonly="readonly">Please select</option>
                        <?php foreach($provinces as $name): ?>
                        <option value="<?php echo $name ?>"><?php echo $name ?></option>
                        <?php endforeach; ?>
                    </select>
                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                </div>              
                
                <div class="row">
                    <label>date of Birth <span class="sub">(Open to 12-25 years old only)</span></label>
                    <input type="text" class="datebirth" name="dob"  id="datepicker" data-error="This field is required"/ data-required readonly>
                </div>

                <div class="row">
                <label>Mobile number</label>
                <input type="tel" name="mobile_number" data-error="This field is required" data-label="number" data-required onkeypress="return isNumberKey(event)" required-number="11"/>
                

				<div class="checkbox-container">
	                <label class="custom-field checkbox">
	                	<span class="labeld">I have read and agree to the <a target="blank" href="<?php echo  $terms_url ?>">terms of use</a></span>
	                    <input type="checkbox" type="checkbox" name="agree_to_terms" data-error="Please check the box before proceeding." data-required />
	                    <div class="indicator"></div>
	                </label>
	            </div>


                <p>We collect your personal data to give you access to our campaign promo, “Courage Unlocked”. </p>
                <p>If you do not provide your information, you would be unable to enter this promotion.  Your personal data may be shared with our affiliates or contacted service providers who manage aspects of this promotion for us.  You have a right, subject to certain exceptions, to request access of, or correction to your personal data.  For full details on how we use your personal data, please read our Complete Promo Mechanics.</p>
                <p>By clicking “SUBMIT”, you expressly consent to the processing of your personal data as described here.</p>
            </fieldset>
            <button type="submit" class="submit-button">Submit <i class="fa fa-caret-right" aria-hidden="true"></i></button>
        </form>
    </div>
</section>
