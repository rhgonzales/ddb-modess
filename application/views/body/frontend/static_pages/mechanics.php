	<section class="page-background-container mechanics">
		<div class="page-background-wrapper"></div>

			<div class="mechanics-wrapper">
				<h1>mechanics</h1>
				<div class="small-12 step-1">
					<div class="step-content">
						<div class="step-content-img text-center">
							<a id="image-participating-products" href="<?php echo base_url('participating-products') ?>" class="trackme">
								<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/MECHANICS-01_03.png')?>">
							</a>
						</div>
						<div class="step-content-desc">
							<p>Buy any <a id="link-participating-products" href="<?php echo base_url('participating-products') ?>" class="trackme">MODESS&reg;, CLEAN & CLEAR&reg;, or CAREFREE&reg; product</a> within the promo period. Don’t forget to keep the receipt(s) and product package! Once you’ve purchased a product, you can register your personal details. </p>
						</div>
					</div>
				</div>
				<div class="small-12 step-2">
					<div class="step-content">
						<div class="step-content-img text-center">
							<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/MECHANICS-03_03.png')?>">
						</div>
						<div class="step-content-desc text-center">
							<p>After registering, upload or <br/> take a selfie and apply any of <br/> the 10 #CourageUnlocked <br/> filters on the photo.</p>
						</div>
					</div>
				</div>
				<div class="small-12 step-3">
					<div class="step-content">
						<div class="step-content-img text-center">
							<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/MECHANICS-02_03.png')?>">
						</div>
						<div class="step-content-desc text-center">
							<p>Upload the <br/> #CourageUnlocked-filtered selfie.<br/> You can also share it on your <br/>Facebook and Twitter pages! </p>
						</div>
					</div>
				</div>



				<div class="small-12 text-center ending-text">
				<div class="step-content">
					<p>The more #CourageUnlocked selfies 
						you share, the more chances of winning. 
						So, keep those photos coming!</p>
						<a id="link-join-now" href="<?php echo $take_photo ?>" class="button full radical trackme">Upload Selfie</a>
						<a id="link-terms-and-conditions" href="<?php echo base_url('terms-and-conditions') ?>" class="button full lightblue trackme">Read terms and conditions</a>
						<a id="link-home" href="<?php echo $home_link ?>" class="button full lightblue trackme">back to home</a>
						</div>
				</div>

				
			</div>
	</section>
