<section class="page-background-container filter">
	<div class="page-background-wrapper"></div>
		<div class="content-wrapper product-wrapper">
			<h1>Participating products</h1>

			<div class="intro-banner-block text-center">
				<p>Buy any of the following products</p>
			</div>

			<div class="product-container clearfix" id="tab-container">
				<ul class="tabs-title clearfix">
					<li class="tab-title active modess" data-tab="tab-1">
						<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brqand-modess_07.png')?>">
					</li>
					<li class="tab-title cleanclear" data-tab="tab-2">
						<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brand-clean&clear_06.png')?>">
					</li>
					<li class="tab-title carefree" data-tab="tab-3">
						<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brand-carefree_07.png')?>">
					</li>
				</ul>

				<div class="tabs-content clearfix">
					<div class="tab-content tab-1 modess active clearfix">
					    <ul class="product-slider">
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/modess-all-night-4_03.png')?>">
					    		<p>MODESS&reg; All Night 4s</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/modess-all-night-10_03.png')?>">
					    		<p>MODESS&reg; All Night 10s</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/modess-cottony-soft-nwings_03.png')?>">
					    		<p>MODESS&reg; Cottony Soft Non-Wings 8s</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/modess-cottony-soft-wings_03_03.png')?>">
					    		<p>MODESS&reg; Cottony Soft with Wings 8s</p>
					    	</li>
					    </ul>
					</div>
					<div class="tab-content tab-2 cleanclear clearfix">
				  		<ul class="product-slider">
				  			<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/FFW-BOTTLE-20-mL.png')?>">
					    		<p>CLEAN &amp; CLEAR&reg; <br/> Foaming Facial Wash 20mL</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/FFW-BOTTLE-50-mL.png')?>">
					    		<p>CLEAN &amp; CLEAR&reg; <br/> Foaming Facial Wash 50mL</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/ccfoamingfacewash_large.png')?>">
					    		<p>CLEAN &amp; CLEAR&reg; <br/> Foaming Facial Wash 100mL</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/OCF 60sts.png')?>">					    		
					    		<p>CLEAN &amp; CLEAR&reg; <br/> Oil Control Film 60 Sheets</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/OCF-50sts.png')?>">
					    		<p>CLEAN &amp; CLEAR&reg; <br/> Oil Control Film 50 Sheets</p>
					    	</li>
					    </ul>
					</div>
					<div class="tab-content tab-3 carefree clearfix">
				  		<ul class="product-slider">
				  			<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/breathable20s.png')?>">
					    		<p>CAREFREE&reg; Breathable 20s</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/breathable40s.png')?>">
					    		<p>CAREFREE&reg; Breathable 40s</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/superdry20s.png')?>">
					    		<p>CAREFREE&reg; Super Dry 20s</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/superdry40s.png')?>">
					    		<p>CAREFREE&reg; Super Dry 40s</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/HF20s.png')?>">
					    		<p>CAREFREE&reg; Healthy Fresh 20s</p>
					    	</li>
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/HF40s.png')?>">
					    		<p>CAREFREE&reg; Healthy Fresh 40s</p>
					    	</li>					    	
					    	<li class="product-item">
					    		<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/AFH20s.png')?>">
					    		<p>CAREFREE&reg; Acti-Fresh Healthy 20s</p>
					    	</li>
					    </ul>
					</div>
				</div>
				
				<p class="text-center">
					<a id="link-home" href="<?php echo base_url()?>" class="button block blue trackme">home</a>
				</p>
			</div>
			
		</div>
</section>
