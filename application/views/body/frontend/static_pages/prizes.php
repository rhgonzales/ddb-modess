<section class="full-width-container prizes-container clearfix">
	<div class="prizes-carousel medium-6">
		<div class="prizes-slide" id="prizes-slide">
			<div class="slide-item" style="background: url('<?php echo base_url('assets/theme/FE/dist/assets/img/Photo-195.jpg')?>') center/cover no-repeat;">
				<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/Photo-195.jpg')?>" style="visibility: hidden;">
			</div>
			<div class="slide-item" style="background: url('<?php echo base_url('assets/theme/FE/dist/assets/img/Photo-145.jpg')?>') center/cover no-repeat;">
				<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/Photo-145.jpg')?>" style="visibility: hidden;">
			</div>
			<div class="slide-item" style="background: url('<?php echo base_url('assets/theme/FE/dist/assets/img/Photo-72.jpg')?>') center/cover no-repeat;">
				<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/Photo-72.jpg')?>" style="visibility: hidden;">
			</div>
			<div class="slide-item" style="background: url('<?php echo base_url('assets/theme/FE/dist/assets/img/Photo-37.jpg')?>') center/cover no-repeat;">
				<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/Photo-37.jpg')?>" style="visibility: hidden;">
			</div>
		</div>
	</div>
	<div class="prizes-desc medium-6">
		<div class="intro-container text-center">
			<h1>What exciting prizes are <br class="break">up for grabs?</h1>
			<p>5 lucky girls will get to join Liza 
			on an adventure in the sky at
			<span class="title-span">Masungi Georeserve</span></p>
			<a id="link-home" href="<?php echo $login_url ?>" class="button full radical trackme">JOIN NOW</a>
		</div>

		<div class="product-prizes-wrapper">
			<h2 class="text-center">other Prizes
			<span class="sub-title">(Actual prizes may vary from the images provided in this page.)</span></h2>
			

			<div class="product-prizes-items-overflow">
				<div class="product-prizes-items">
					<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/prizes.jpg')?>" class="mobile-view">
					
					<div class="go-pro-prize prizes">
						<div class="rel">
							<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/gopro-prize_06.png')?>">
							<span>GoPro<br>Hero 5</span>
						</div>
					</div>
					<div class="tent-prize prizes">
						<div class="rel">
							<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/tent-prize_06.png')?>">
							<span>Camping<br>Tent</span>
						</div>
					</div>
					<div class="jacket-prize prizes">
						<div class="rel">
							<span>The North Face<br>Jacket Gift<br> Certificate</span>
							<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/jacket-prize_06.png')?>">
						</div>
					</div>
					<div class="bag-prize prizes">
						<div class="rel">
							<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/bag-prize_07.png')?>">
							<span>Deuters<br>Bag</span>
						</div>
					</div>
					<div class="tumbler-prize prizes">
						<div class="rel">
							<span>Klean<br>Kanteen<br>Tumbler</span>
							<img src="<?php echo base_url('assets/theme/FE/dist/assets/img/tumbler-prize_10.png')?>">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
