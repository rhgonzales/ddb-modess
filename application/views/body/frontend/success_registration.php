<section class="page-background-container registration">
    <div class="page-background-wrapper">	</div>
        <div class="pagination-wizard show-for-medium">
              <span class="wizard-indicator active" arial-hidden="true"></span>
              <span class="wizard-indicator" arial-hidden="true"></span>
              <span class="wizard-indicator" arial-hidden="true"></span>
          </div>
    <div class="content-wrapper">
        <div class="pagination-wizard hide-for-medium">
              <span class="wizard-indicator active" arial-hidden="true"></span>
              <span class="wizard-indicator" arial-hidden="true"></span>
              <span class="wizard-indicator" arial-hidden="true"></span>
          </div>
        <div class="success-message-container text-center">
            <h2>congratulations!</h2>
            <p>You can now begin <br/>unlocking courage by <br/>uploading photo entries!</p>
            <p>To make them valid, just get any <br/> of our <a href="<?php echo $product_page ?>" class="trackme">participating products</a><br>and keep the receipt and package!</p>
        </div>
        <div class="content-container text-center">
            <a href="<?php echo $home_link;  ?>" class="button block pink trackme">HOME</a>
            <a href="<?php echo $take_photo; ?>" class="button block deluge trackme">Upload PHoto</a>
        </div>
    </div>
</section>
