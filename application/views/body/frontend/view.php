<section class="page-background-container gallery">
	<div class="page-background-wrapper"></div>
		<div class="content-wrapper text-center photo-gallery-inside">
			<div class="row photo-gallery-wrapper">
				<div class="small-12 gallery-photo-image">
					<img src="<?php echo $entry->image?>">
				</div>
				<div class="small-12">
					<div class="gallery-photo-desc">
						<h2>By <span class="name"><?php echo $entry->first_name?> <?php echo $entry->last_name?></span></h2>
						<?php //Location: application/views/tpls/frontend/partials/gallery_detail_tpl.php ?>
						<?php echo $share_tpl; ?>
					</div>

					<div class="gallery-photo-desc-buttons">
						<?php if(isset($login_link)) {?>
						<p>Want to be 1 of 5 lucky girls <br/> to join Liza on an adventure in the sky?</p>
						<a href="<?php echo $login_link ?>" class="button block cornflower trackme">Log in</a><br/>
						<a href="<?php echo $mechanics_link ?>" class="button block seance trackme">views mechanics</a><br/>
						<?php }?>
						<a href="<?php echo $home_link ?>" class="button block plum trackme">home</a>
					</div>
				</div>
			</div>

		</div>
</section>
