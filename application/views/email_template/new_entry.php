<p>Hey, girl!</p>
<p>Thanks for sending us your Courage Unlocked selfie!</p>
<p>
Watch out for another email from us soon to find out if your entry is qualified!<br>
Until then, keep buying your favorite MODESS<sup>&reg;</sup>, CAREFREE<sup>&reg;</sup>, and CLEAN & CLEAR<sup>&reg;</sup> products, hold on to the purchase receipts and product packaging, and upload more entries for more chances to unlock courage with Liza Soberano on an adventure in the sky!
</p>
<p>To create more promo entries, go to <a href="http://www.courageunlocked.com/?utm_source=Email&utm_medium=Automated&utm_campaign=Body&utm_term=Home">https://courageunlocked.megstreetwear.ph</a>!</p>
<p style="font-size:12px"><i>Promo runs from April 3 - May 31, 2017.<br>Grand prize winners unlock courage with Liza Soberano in Masungi Georeserve on June 25, 2017.</i></p>
<br>
<p style="text-align: center;font-size: 14px;">
    <a href="<?php echo 'https://www.facebook.com/modessangelsofficial'?>" target="_blank"><img alt="Modess" title="Modess" src="<?php echo base_url('assets/theme/FE/dist/assets/img/brqand-modess_07.png') ?>" style="width: 90px;"></a>
    <a href="<?php echo 'https://www.facebook.com/cleanclearphilippines/' ?>" target="_blank"><img alt="Clean & Clear" title="Clean & Clear" src="<?php echo base_url('assets/theme/FE/dist/assets/img/brand-clean&clear_06.png') ?>" style="width: 90px;"></a>
    <a href="<?php echo 'https://www.facebook.com/CarefreePH/' ?>" target="_blank"><img  alt="Carefree" title="Carefree" src="<?php echo base_url('assets/theme/FE/dist/assets/img/brand-carefree_07.png') ?>" style="width: 90px;"></a>
    <br><br>
    <a href="<?php echo base_url('?utm_source=Email&utm_medium=Automated&utm_campaign=Footer&utm_term=Home') ?>">Home</a>&nbsp;|&nbsp;
    <a href="<?php echo base_url('participating-products?utm_source=Email&utm_medium=Automated&utm_campaign=Footer&utm_term=Products') ?>">Participating Products</a>&nbsp;|&nbsp;
    <a href="<?php echo base_url('prizes?utm_source=Email&utm_medium=Automated&utm_campaign=Footer&utm_term=Prizes') ?>">Prizes</a>&nbsp;|&nbsp;
    <a href="<?php echo base_url('mechanics?utm_source=Email&utm_medium=Automated&utm_campaign=Footer&utm_term=Mechanics') ?>">Contest Mechanics</a>&nbsp;|&nbsp;
    <a href="<?php echo base_url('terms-and-conditions?utm_source=Email&utm_medium=Automated&utm_campaign=Footer&utm_term=TnC') ?>">Terms & Conditions</a>
</p>

<style type="text/css">
    p {
        margin: 25px 0px;
    }
</style>