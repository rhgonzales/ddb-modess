<!doctype html>

<html lang="en">
	<head>
		<title><?php echo $title . config_item('suffix_title') ?></title>
		<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
		<?php if (isset($extra_headers)) echo $extra_headers ?>
		
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="<?php echo base_url('favicon.ico')?>" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url('favicon.ico')?>" type="image/x-icon">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url('assets/theme/FE/dist/assets/css/app.css') ?>">
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
        {'gtm.start': new Date().getTime(),event:'gtm.js'}
        );var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5G34QMQ');</script>
        <!-- End Google Tag Manager -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>        
	</head>
	<body <?php echo isset($body_class) ? "class=\"{$body_class}\"" : ""; ?>>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5G34QMQ"
        height="0" width="0" style="display:none;visibility:hdden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
	<!-- 
		if homepage add class to body and nav element 'home-nav'
		if homepage add class to body for changing navigation color
		homepage = page-index
	-->

		<div id="fb-root"></div>
		<script>(function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id))
					return;
				js = d.createElement(s);
				js.id = id;
				js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=397216887319382";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
		<?php echo $header1 ?>
		<?php echo $nav_menu ?>
		<?php echo $body ?>
		<?php echo $footer ?>
		<script src="<?php echo base_url('assets/theme/FE/dist/assets/js/app.js') ?>"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>
		  $( function() {
			$( "#datepicker" ).datepicker();
		  } );
		</script>
	</body>

</html>
