<!-- Facebook meta-->
        <meta property="og:url" content="<?php echo $url?>" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php echo $title?>" />
        <meta property="og:description" content="<?php echo $description?>" />
        <meta property="og:image" content="<?php echo $image?>" />
        <!-- Twitter meta-->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="<?php echo $title?>">
        <meta name="twitter:description" content="<?php echo $description?>">
        <meta name="twitter:image" content="<?php echo $image?>">
