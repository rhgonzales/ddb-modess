<footer>
    <div class="footer-wrapper clearfix">
        <ul class="brands-list column medium-6">
            <li><a id="image-modess" class="trackme" href="<?php echo $social_links['modess'] ?>" target="_blank"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brqand-modess_07.png') ?>"></a></li>
            <li><a id="image-clean-and-clear" class="trackme" href="<?php echo $social_links['clean_clear'] ?>" target="_blank"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brand-clean&clear_06.png') ?>"></a></li>
            <li><a id="image-carefree" class="trackme" href="<?php echo $social_links['carefree'] ?>" target="_blank"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brand-carefree_07.png') ?>"></a></li>
        </ul>
        <ul class="link-list  column medium-6">
            <li><a id="footer-home" class="trackme" href="<?php echo base_url() ?>">Home</a></li>
            <li><a id="footer-participating-products" class="trackme" href="<?php echo base_url('participating-products') ?>">Participating Products</a></li>
            <li><a id="footer-prizes" class="trackme" href="<?php echo base_url('prizes') ?>">Prizes</a></li>
            <li><a id="footer-mechanics" class="trackme" href="<?php echo base_url('mechanics') ?>">Contest Mechanics</a></li>
            <li><a id="footer-terms-and-conditions" class="trackme" href="<?php echo base_url('terms-and-conditions') ?>">Terms & Conditions</a></li>
        </ul>
    </div>
    <div class="footer-dti">
        <small>
            Per DTI-FTEB Permit No. 3692 Series of 2017<br>
            Per DOH-FDA CCRR Permit No. 208 s. 2017
        </small>
    </div>
</footer>