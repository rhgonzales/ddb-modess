<div class="gallery-photo-desc">
    <ul class="social-share-list">
        <li><a target="_blank" href="<?php echo $sharer ?>"><span class="icon-circle font-icon"><i class="fa fa-facebook" aria-hidden="true"></i></span> share this <br> on facebook</a></li>
        <li><a target="_blank" href="https://twitter.com/share?url=<?php echo $data['url']?>&hashtags=<?php echo $data['hashtags']?>&text=<?php echo $data['text']?>"><span class="icon-circle font-icon"><i class="fa fa-twitter" aria-hidden="true"></i></span>Tweet <br>about it</a></li>
        <li><a href="<?php echo $upload_selfie ?>"><span class="icon-circle selfie"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/helper-icon-menu-filter_06.png')?>"></span>Upload <br>selfie</a></li>
        <li><a target="_blank" href="<?php echo $download_link ?>"><span class="icon-circle save"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/download-icon_03.png')?>"></span>Download</a></li>
        <li><a href="<?php echo $gallery_link ?>"><span class="icon-circle entries"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/gallery-icon_03.png')?>"></span>view <br>entries</a></li>
    </ul>
</div> 
