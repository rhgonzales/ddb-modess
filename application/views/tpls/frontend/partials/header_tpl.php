<div class="navigation-wrapper">
    <div class="navigation-drawer">
    	<div class="guther">
	        <button class="drawer-toggle-button"><i class="fa fa-times" aria-hidden="true"></i></button>
	        <div class="logo-container">
	            <img src="<?php echo base_url('assets/theme/FE/dist/assets/img/courage-logo_03.png') ?>">
	        </div>

	        <ul class="nav-links">
	            <li>
	                 <a id="nav-home" class="trackme" href="<?php echo base_url() ?>"> <img src="<?php echo base_url('assets/theme/FE/dist/assets/img/icon-house_01.png') ?>"> Home</a>
	            </li>
	            <li>
	                <a id="nav-mechanics" class="trackme" href="<?php echo base_url('mechanics') ?>"> <img src="<?php echo base_url('assets/theme/FE/dist/assets/img/mechanics-icon_03.png') ?>"> Read Mechanics</a>
	            </li>
	            <li>
	                <a id="nav-filters" class="trackme" href="<?php echo base_url('filters'); ?>"> <img src="<?php echo base_url('assets/theme/FE/dist/assets/img/icon-viewfilters_01.png') ?>"> View Filters</a>
	            </li>
	            <li>
	                <a id="nav-gallery" class="trackme" href="<?php echo base_url('gallery'); ?>"> <img src="<?php echo base_url('assets/theme/FE/dist/assets/img/icon-gallery_01.png') ?>"> View Gallery</a>
	            </li>
	            <li>
	                <a id="nav-participating-products" class="trackme" href="<?php echo base_url('participating-products'); ?>"> <img src="<?php echo base_url('assets/theme/FE/dist/assets/img/icon-products_01.png') ?>"> Participating Products</a>
	            </li>
	            <li>
	                <a id="nav-prizes" class="trackme" href="<?php echo base_url('prizes'); ?>"> <img src="<?php echo base_url('assets/theme/FE/dist/assets/img/icon-prizes_01.png') ?>"> Prizes</a>
	            </li>
	            <li>
	                <a id="nav-join-now" class="trackme" href="<?php echo $join_now_link ?>"> <img src="<?php echo base_url('assets/theme/FE/dist/assets/img/icon-join_01.png') ?>"> Upload Selfie</a>
	            </li>
	        </ul>
	        
		</div>
			<?php
				/*
				 * <a href="<?php echo $social_links['modess'] ?>" target="_blank"></a>
				 * <a href="<?php echo $social_links['carefree'] ?>" target="_blank"></a>
				 * <a href="<?php echo $social_links['clean_clear'] ?>" target="_blank"></a>
				 */
 			?>
		<div class="navigation-footer">
			<ul class="brands-list column medium-6">
				<li><a id="image-modess" class="trackme" href="<?php echo $social_links['modess'] ?>" target="_blank"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brqand-modess_07.png') ?>"></a></li>
				<li><a id="image-clean-and-clear" class="trackme" href="<?php echo $social_links['clean_clear'] ?>" target="_blank"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brand-clean&clear_06.png') ?>"></a></li>
				<li><a id="image-carefree" class="trackme" href="<?php echo $social_links['carefree'] ?>" target="_blank"><img src="<?php echo base_url('assets/theme/FE/dist/assets/img/brand-carefree-white_07.png') ?>"></a></li>
			</ul>
	    </div>
    </div>
</div>
