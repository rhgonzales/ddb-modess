
(function($){
	'use strict';

	var root,
		module = {

		init: function(){
			// init path
			root = this;

			//DOM caching
			root.$navDrawer = $('.drawer-toggle-button'),
			root.$nav = $('nav'),
			root.$body = $('body'),
			root.$formSubmit = $('.submit-button'),
			root.$form = $('.registration-form'),
			root.$formInput = $("input:not([type='radio']):not([type='checkbox']):not([type='submit']):not([type='image']):not([type='file']):not([type='tel']):not([type='hidden']), select"),
			root.$checkInput = $("input[type='checkbox']");
			root.$phoneInput = $("input[type='tel']");
			root.$formInputValidate = root.$form.find($('input'));
			root.$formSelectValidate = root.$form.find($('select'));
			root.$anchorLink = $('a[href*="#"].anchor-arrow');
			root.$bannerBlock = $('.banner-block');


			// function invoking
			root.$navDrawer.on('click', root.navigation);
			root.$form.on('submit', root.formValidation);
			root.$formInput.on('focus', root.focusInputValidate);
			root.$formInputValidate.on('focusout', root.validateInputData);
			root.$anchorLink.on('click', root.smoothScroll);

			if (root.$body.hasClass('ended_promo')) {
				root.endedPromo()
			}		
			

			root.navigationScroll();
		},

		endedPromo: function(e) {
			root.$body.find('#ended-promo').addClass('revealed')
			root.$body.find('.filter-reveal-container').addClass('revealed')
			root.$body.css('overflow', 'hidden')
			
			$('#ended-promo').find('[data-close]').on('click', function(){
				root.$body.find('#ended-promo').removeClass('revealed')
				root.$body.removeAttr('style')
			})
		},

		navigation: function(e){
			e.preventDefault();
			$('.navigation-drawer').toggleClass('is-visible');
			$('.navigation-wrapper').toggleClass('open-nav');		
		},

		navigationScroll: function(){
			if (root.$body.hasClass('home-nav')){
				$(window).scroll(function(){ 
					var scroll = $(window).scrollTop();                         
		            if (scroll >= 75) {
			            root.$nav.removeClass('home-nav').addClass("scrolled");
			        } else {
			            root.$nav.removeClass("scrolled").addClass('home-nav');
			        }
		        });
			}			
		},

		smoothScroll: function(e){
			e.preventDefault();
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html, body').animate({
		          scrollTop: target.offset().top - 85
		        }, 1000);
		        return false;
		      }
		    }
		},

		// function by clicking submit button
		formValidation: function(){

			// remove error message on triggered
			$(this).find('span.error-message').remove();
			root.$bannerBlock.hide();

			root.isInputError = true;
			root.ischeckError = true;
			root.isnumberError = true;

			var inputValidLength = 0,
				inputLength = $(this).find(root.$formInput).length

			// check input fields if it has data
			$(this).find(root.$formInput).each(function(){
				if(!$(this).val()){
					var errorMsgs = $(this).data("error");
					$(this).prev('label').after('<span class="error-message">'+ errorMsgs +'</span>');
				} else {
					inputValidLength++;
				}
			})


			if(inputValidLength === inputLength) {
				root.isInputError = false;
			}


			$(this).find(root.$phoneInput).each(function(){
				if(!$(this).val()){
					var errorMsgs = $(this).data("error");
					$(this).prev('label').after('<span class="error-message">'+ errorMsgs +'</span>');
					root.isnumberError = true;
				} else {
					var errorMsgs = $(this).data("error");
					$(this).prev('label').after('<span class="error-message">Invalid</span>');
					var reqNum = parseInt($(this).attr('required-number'));
					if($(this).val().length == reqNum){
						$(this).prev('span.error-message').remove();
						root.isnumberError = false;
					}
				}
			})			

			// check checkbox is checked
			$(this).find(root.$checkInput).each(function(){
				if(!$(this).is(':checked')){
					var errorMsgs = $(this).data("error");
					$(this).parent().find('span').after('<span class="error-message">'+ errorMsgs +'</span>');
					root.ischeckError = true;
				} else {
					$(this).parent().next('span.error-message').remove();
					root.ischeckError = false;
				}
			})


			// validate if error still exist
			if( root.isInputError === false && root.ischeckError === false && root.isnumberError == false){
				return true;
			} else {
				$('.error-container-message').show();
				$('.submit-button').addClass('error');
				$("html, body").animate({scrollTop: 0}, 1000);
				return false;
			}
		},

		focusInputValidate: function(){		
			// remove error message on focus
			$(this).prev('span.error-message').remove();				
		},

		validateInputData: function() {
			$(this).each(function(){
				if($(this).attr('data-label') == "email"){
					var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
						is_email = re.test($(this).val());
					if(!is_email){
						$(this).prev('label').after('<span class="error-message">Invalid</span>');
						root.isInputError = true;
					} else {
						root.isInputError = false;
					}
				}

				if($(this).attr('data-label') == "name"){
					if(!$(this).val().match('^[a-zA-Z-\s]{3,16}$')){
						$(this).prev('label').after('<span class="error-message">Invalid</span>');
						root.isInputError = true;
					} else {
						root.isInputError = false;
					}
				}

				if($(this).attr('data-label') == "phone"){
					var reqNum = parseInt($(this).attr('required-number'));
					if($(this).val().length == reqNum){
						$(this).prev('span.error-message').remove();
						root.isnumberError = false;			
					} else{
						$(this).prev('label').after('<span class="error-message">Invalid</span>');
						root.isnumberError = true;	
						
					}
				}
			})
		}



	};

	$(function(){	
		$(document).foundation();
		$('.homepage-slider').slick({
			dots: true,
			prevArrow: $('.prev-arrow'),
      		nextArrow: $('.next-arrow'),
      		//adaptiveHeight: true,
      		//autoplay: true
		});


		$("#datepicker").datepicker({
			changeMonth: true,
      		changeYear: true,
      		showButtonPanel: false,
			yearRange: "1991:2005",
			minDate: new Date(1991, 5, 1),
			maxDate: new Date(2005, 4, 31),
			defaultDate: "01/01/2000"
			// onSelect: function(value) {
			// 	var today = new Date(),
		 //            dob = new Date(value),
		 //            age = new Date(today - dob).getFullYear() - 1970;
			// 	alert(age)
			// }
		});


		$('#prizes-slide').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			autoplay: true,
  			autoplaySpeed: 5000,
		});
		
		$(window).resize(function(){
			var winWidth = $(this).width(),
				prizeSlideHeight = $('#prizes-slide').parent().outerHeight()

			if (winWidth < 990) {
				$('#prizes-slide').css('height', 'auto').slick('reinit');
			} else {
				$('#prizes-slide').height(prizeSlideHeight).slick('reinit');
			}
		}).resize();


		module.init();
	});

})(jQuery);
