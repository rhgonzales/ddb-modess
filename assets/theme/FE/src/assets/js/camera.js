// /*
// Author: george.e.jr@live.com
// Description: html5 capture photo
// Dependencies: HTML5
// */

var ddbApp = ddbApp || {};

(function($){
      var root, 
          cameraMode = {
          init: function(){
            cameraMode.resizeCam();
            
            var canvas = document.getElementById('canvas'),
                context = canvas.getContext('2d'),
                camera = document.getElementById('camera'),
                cWidth,
                cHeight;
            root = this;
            root.$editPhotoTab = $(".edit-photo");
            root.$takePhotoTab = $(".take-photo");

            //access cam
            if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                    camera.src = window.URL.createObjectURL(stream);
                    camera.play();
                });
            }
            cameraMode.capture(context,camera);
            cameraMode.selectFilter();
          }, 
          capture: function(context,camera){
              $('#capture').bind('click', function() {
                var _canvasWidth =  $('#canvas').attr('width'),
                    _canvasHeight =  $('#canvas').attr('height');
                alert("captured");
                console.log('camera', camera)
                context.drawImage(camera, 0, 0, 1000, 1000);
							
								var dataURL = $('#canvas')[0].toDataURL()
								console.log(dataURL)

                cameraMode.convertCanvas2Img(canvas);
                $("#canvas").hide();
                $("#select-frame-label").click();
                cameraMode.loadFilters();
                root.$editPhotoTab.show();
                root.$takePhotoTab.hide();
                //convert edited to canvas
                // cameraMode.outputFilter();
            }); 
          },
          resizeCam: function(){
              $(window).on('load', function () {
                    if ($(window).width() >= 768){
                        $('#camera,#canvas').attr({
                            width: 640,
                            height: 480
                        });
                    }else if($(window).width() < 768){
                        $('#camera,#canvas').attr({
                            width: 320,
                            height: 240
                        });
                    }else if($(window).width() >= 1024){
                        $('#camera,#canvas').attr({
                            width: 800,
                            height: 600
                        });
                    }
              });
          },
          convertCanvas2Img: function(canvas) { 
                var image = new Image();
                image.src = canvas.toDataURL("image/png");
                // console.log(image.src); 
                $('#canvas-output').attr("src",image.src);
          },
          loadFilters: function(){ 
                $(document).ready(function(){
                    $('.filters').slick({  
                        slidesToShow: 3,
                        slidesToScroll: 1,
                         lazyLoad: 'ondemand',
                         dots: false,
                         speed: 300,
                         arrows: true,
                         prevArrow: '<span class="prev"><i class="fa fa-caret-left" aria-hidden="true"></i></span>',
                         nextArrow: '<span class="next"><i class="fa fa-caret-right" aria-hidden="true"></i></span>'
                    });
                });
          },
          outputFilter: function(){ 
                html2canvas(document.getElementById("output-filter"), {
                    onrendered: function (canvas) {
                        document.body.appendChild(canvas);
                    },
                    width:640,
                    height:480
                });
          },
          selectFilter: function() {
             $('.filters img').click(function(e){
                 console.log("filter click");
                e.preventDefault();
                var selectedFilter =  $(this).attr('data-name');
                $('img.filter').attr('src', '/assets/img/filters/' + selectedFilter + '.png');
              });
          }
      };
      
      $(function(){
          //cameraMode.init();
      });

 })(jQuery);
