/*
Module Name:    Load Gallery
Author:         Chad Jordan Hilis
Description:    Custom tabs for product page with fixed slick carousel bug on initialized
Dependencies:   jQuery & slick carousel
Version:        1.0 and up
*/

(function($){
    'use strict';

    var root,
        productTabs = {
            init: function() {   
                if (!$('#tab-container').length) return false

                // check if file is supported by current user's browser
                if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
                    root._notSupported()
                    return
                }

                // init path
                root = this
                
                root.$productContainer = $('#tab-container')
                root.$tabTitle = root.$productContainer.find('li.tab-title')
                root.$tabContent = root.$productContainer.find('.tab-content')
                root.sliderConfig = {
                    speed: 300,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    prevArrow: '<button class="slick-arrow prev"slick-prev" id="#arrow-prev"><i class="fa fa-caret-left" aria-label="true"></i></button>',
                    nextArrow: '<button class="slick-arrow next"slick-next" id="#arrow-prev"><i class="fa fa-caret-right" aria-label="true"></i></button>',
                    responsive: [
                        {
                          breakpoint: 768,
                          settings: {
                            centerMode: true,
                            slidesToShow: 1
                          }
                        }
                      ]
                }

                root.tabs();
            },

            tabs: function() {

                root.$productContainer.find('.tab-title').on('click', root._handleTabs)

                // if (root.$productContainer.find('.product-slider'))
                //     root.$productContainer.find('.product-slider').slick(root.sliderConfig);
            },

            _handleTabs: function() {

                var tabSelected = $(this).attr('data-tab')

                root.$productContainer.find('.tab-title.active').removeClass('active')
                root.$tabContent.removeClass('active')

                root.$productContainer.find('[data-tab="'+ tabSelected +'"]').addClass('active')
                root.$productContainer.find('.tab-content.' + tabSelected).addClass('active')

                // needs reworks

                // if (root.$productContainer.find('.tab-1 .product-slider').hasClass('slick-initialized')) {
                //     root.$productContainer.find('.tab-1 .product-slider').slick('unslick')
                // }

                // if (root.$productContainer.find('.tab-2 .product-slider').hasClass('slick-initialized')) {
                //     root.$productContainer.find('.tab-2 .product-slider').slick('unslick')
                // }

                // if (root.$productContainer.find('.tab-3 .product-slider').hasClass('slick-initialized')) {
                //     root.$productContainer.find('.tab-3 .product-slider').slick('unslick')
                // }

                // if (root.$productContainer.find('.product-slider'))
                //     root.$productContainer.find('.product-slider').slick(root.sliderConfig);
            },


        };
		


    $(function(){
        productTabs.init();
    });

})(jQuery);
