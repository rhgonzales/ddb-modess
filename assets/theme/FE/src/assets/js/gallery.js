/*!
Module Name:    Load Gallery
Author:         Chad Jordan Hilis
Author URI:     http://www.royanonuevo.com/
Description:    Image request using ajax for gallery
Dependencies:   jQuery
Version:        1.0 and up
*/

(function($){
    'use strict';

    var root,
        loadGallery = {
            init: function() {   
                if (!$('.gallery-thumbs-list').length) { return }

                // check if file is supported by current user's browser
                if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
                    root._notSupported()
                    return
                }

                // init path
                root = this
                
                root.tabContent = $('.tabs-content');
                root.loadMore = root.tabContent.find('.load-more');
                root.loadTail = $('.helper-menu-container:before');
               
                
                
                root.initializeFetchData();                
                root.loadMore.on('click', function(e){
                    e.preventDefault();
                    root.fetchData($(this).closest('.tab-content'));
                });

            },

            // initialize first load of data
            initializeFetchData: function(){
                var tabsPanel = root.tabContent.find('.tab-content');
                if(tabsPanel.length){
                    tabsPanel.each(function(){
                        root.fetchData($(this));
                    });
                }
            },

            fetchData: function(elem){   

                var dataUrl = elem.data('url'),
                    ListContainer = elem.find('.gallery-thumbs-list');

                if(dataUrl){
                    $.ajax({
                        type: 'GET',
                        url: dataUrl,
                        dataType: 'json',
                        success: function(responseData) {
                            var items = responseData.data,
                                html = '';

                                    // each items in json
                                    $.each(items, function(key, object){
                                        html = '<li><a href="' + object.entry_link + '"><img src="' + object.image + '"></a></li>';
                                        // append to DOM
                                        ListContainer.append(html);                           
                                    });

                                    if (responseData.hasOwnProperty('next_page_url')) {
                                        elem.data('url',responseData.next_page_url);
                                    }else{
                                        elem.find('.load-more').text('');
										elem.data('url','');
                                    }              
                        },
                        error: function(responseData){
                             elem.find('.load-more').text('');
							elem.data('url','');
                        }
                    });

                }

                 
            },
        };
		


    $(function(){
        loadGallery.init();
    });

})(jQuery);
