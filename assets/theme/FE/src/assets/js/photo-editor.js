/*!
Module Name:    Image Editor
Author:         Roy Anonuevo
Author URI:     http://www.royanonuevo.com/
Description:    Manipulate Image using HTML 5
Dependencies:   jQuery, Load Image - https://github.com/blueimp/JavaScript-Load-Image
Version:        1.0
*/

(function($){
    'use strict'

    var root,
        photoEditor = {
            init: function() {   
                if (!$('#photo-editor').length) { return }
				
				// init path
                root = this

                // plugin options
                root.options = {
                    backgroundColor: '#FFF', // or custom color like '#FFF'
                    imageType: 'image/png', // do not alter because JPEG tyoe does not support a transparent background.
                    resizeImageAfterUpload: true,
                    responsive: true,
                    showCopy: false,
                    width: 900,
                    height: 900
                }

                // global variables
                root.rotateDeg = 0
                root.resizeValue = 0
                root.frame = null
                root.image = null
                root.imageCopy = null
                root.interval = null
                root.frames = []
                root.frameSelectedKey = null
                root.routes = ['take-photo', 'select-frames', 'edit-photo', 'submit', 'confirmation']
                root.routeCurrentIndex = 0
                root.widthPercentage = 100
                root.tabletSize = 1024
                root.widthPercentage = 100
                root.stream = null

                // cache dom
                root.$window = $(window)
                root.$body = $('body')
                root.$pageArrowIndicator = $('.arrow-down-indicator')
                root.$photoEditor = $('#photo-editor')
                //root.$rotateSlider = root.$photoEditor.find('#rotate-slider')
                //root.$resizeSlider = root.$photoEditor.find('#resize-slider')
                root.$fileInput = root.$photoEditor.find('#filesToUpload')
                root.$fileInputText = root.$photoEditor.find('#fileInputText')
                root.$preloader = root.$photoEditor.find('#preloader')
                root.$canvas = root.$photoEditor.find('#canvas')
                root.$video = document.getElementById('video')
                root.$menu = root.$photoEditor.find('.menu ul')
                root.$menus = root.$photoEditor.find('.menu ul li')
                root.$pagination = root.$photoEditor.find('ul#pagination')
                root.$paginateBackBtn = root.$photoEditor.find('li#paginate-back')
                root.$paginateNextBtn = root.$photoEditor.find('li#paginate-next')
                root.$tools = root.$photoEditor.find('#tools')
                root.$resetImage = root.$photoEditor.find('.reset-image-btn')
                root.$zoomInBtn = root.$photoEditor.find('#zoom-in-image')
                root.$zoomOutBtn = root.$photoEditor.find('#zoom-out-image')
                root.$rotateBtn = root.$photoEditor.find('.rotate-btn')
                root.$frames = root.$photoEditor.find('#frames')
                root.$framesJSON = $("#frames-json")
                root.$submitPhoto = root.$photoEditor.find('#submit-photo')
				root.$modalOverlay = root.$photoEditor.find('#modal-overlay')
				root.$configURL = root.$photoEditor.find('#config-url')
				root.$configToken = root.$photoEditor.find('#config-token')
				//root.$shareFacebookbtn = root.$photoEditor.find('#share-facebook-btn')
				//root.$shareTwitterbtn = root.$photoEditor.find('#share-twitter-btn')
				root.$shareBtn = root.$photoEditor.find('#share-btn')
				root.$resultPhoto = root.$photoEditor.find('#result-photo')
				root.$toolEditPhoto = root.$photoEditor.find('#tool-edit-photo')
				root.$webcamCaptureBtn = root.$photoEditor.find('#webcam-capture-btn')
				root.$webcamCloseBtn = root.$photoEditor.find('#webcam-close-btn')

				// show initial page
                root._changeRoute('take-photo')


                // check if file is supported by current user's browser
                if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
                	root._showModal('The File API\'s are not fully supported in this browser. <br>Please use a latest browser version.')
                    return
                } else {
					//root._showModal(false)
					root.$photoEditor.addClass('initialized')
                }
				
				root.widthPercentage = (root.$canvas.outerWidth() / root.$canvas.offsetParent().width()) * 100;

                // load all frames
                root._loadFrames()

               	root.widthPercentage	= (root.$canvas.outerWidth() / root.$canvas.offsetParent().width()) * 100;

                // register events
                root.$window.resize(root._winResize).resize()
                root.$window.scroll(root._winScroll).scroll()
                //root.$rotateSlider.on('moved.zf.slider', root._rotateImage)
                //root.$resizeSlider.on('moved.zf.slider', root._resizeImage)
                root.$fileInput.on('change', root._selectFile)
                root.$resetImage.on('click', root._resetImage)
                //root.$menus.on('click', root._handleMenu)
                root.$frames.find('li').on('click', root._selectFrame)
				root.$photoEditor.find('.pagination-btn').on('click', root._paginate)
				root.$photoEditor.find('#submit-photo').on('click', root._saveImage)
				root.$rotateBtn.on('click', root._handleRotateBtn)
				root.$toolEditPhoto.find('.tool-btn').on('click', root._handleEditPhotoTools)
				root.$photoEditor.find('.tool-btn.tool-btn-webcam').on('click', root._handleWebcam)
				root.$webcamCaptureBtn.on('click', root._handleWebcamCapture)
				root.$webcamCloseBtn.on('click', root._handleWebcamClose)
				
                root.$zoomInBtn.on({
                    'touchstart mousedown': function(e) { 
                        e.preventDefault()
                        root.interval = window.setInterval(function(){ 
                            root._zoomImage(2) 
                        }, 1)
                    },
                    'touchend mouseup mouseleave': function(e) {
                        e.preventDefault();
                        window.clearInterval(root.interval)
                    }
                })

                root.$zoomOutBtn.on({
                    'touchstart mousedown': function(e) { 
                        e.preventDefault();
                        root.interval = window.setInterval(function(){ 
                            root._zoomImage(-2) 
                        }, 1)
                    },
                    'touchend mouseup mouseleave': function(e) {
                        e.preventDefault();
                        window.clearInterval(root.interval)
                    }
                })
            },


            _handlePageArrowIndicator: function() {
            	if (root.$pageArrowIndicator.hasClass('disabled')) return

				var winWidth = root.$window.outerWidth(),
					winHeight = root.$window.outerHeight(),
					winScrollTop = root.$window.scrollTop()
				
				
				if (winHeight < 600 && winWidth && 450  && winScrollTop < 20)
					root.$pageArrowIndicator.addClass('show')
            	else
            		root.$pageArrowIndicator.removeClass('show')	            
            },
			
			_loadFrames: function() {
				var templates = '',
					frames = root.$framesJSON.html(),
					framesArr = $.parseJSON(frames),
					framesLength = framesArr.length
				
				// put array in global variable
				root.frames = framesArr

				for (var i = 0; i < framesLength; i++) {
					var id = framesArr[i].id,
						name = framesArr[i].name,
						img = framesArr[i].img

				    templates += `<li data-frame-img="${img}" data-frame-id="${id}" data-key="${i}">
				                    <img src="${img}" alt="${name}" />
				                    <div class="name">${name}<div/>
				                </li>`
				}
				
				// show frames
				root.$frames.html(templates)
			},
			
			_winScroll: function() {
				root._handlePageArrowIndicator()
			},

            _winResize: function() {
            	root._log('window resized..')
				
            	if (root.options.responsive !== true) return false
				
				var winWidth = root.$window.outerWidth(),
					winHeight = root.$window.outerHeight(),
					canvasWidth = 500,
					oldWidth = root.$canvas.outerWidth(),
					width = root.$canvas.offsetParent().width() * (root.widthPercentage / 100),
					factor = 1
					//factor = width / oldWidth

        		root._handlePageArrowIndicator()

				if (winWidth <= root.tabletSize) {
					root.$photoEditor.addClass('mobile')

            		canvasWidth = winWidth > 500? 500 : winWidth
            	} else {
            		root.$photoEditor.removeClass('mobile')
            	}

            	root.$canvas.css({height: canvasWidth, width: canvasWidth})
				


				if (!root.image) return false
            	root.image
        				.width(root.image.width() * factor)
        				.height(root.image.height() * factor)
		        		.css({
		        			left: root.image.css('left').replace(/[^-\d\.]/g, '') * factor + 'px',
		        			top: root.image.css('top').replace(/[^-\d\.]/g, '')  * factor + 'px'
		        		})
        		
        		root._imageCopy()	
            },

            _handleWebcam: function(e) {
            	e.preventDefault()
				
				root._clearCanvas()

            	var video = root.$video,
            		errMsgs = "Looks like your browser doesn't support webcam api. Please try another browser like Google Chrome.",
					isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/)
				
				if (isSafari) {
					root.$photoEditor
						.addClass('show-webcam-error')
						.removeClass('show-webcam')
						.find('.inside-area.video-error div').html(errMsgs)
					return
				}

				var errCallBack = function(err) {
					if (err.name === 'DevicesNotFoundError') {
						errMsgs = "Sorry, we can't detect your webcam."
						
						root.$photoEditor
								.addClass('show-webcam-error')
								.removeClass('show-webcam')
								.find('.inside-area.video-error div').html(errMsgs)
					}
				}

				// get access to the camera
				if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
	                navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
	                    root.$photoEditor.removeClass('show-webcam-error').addClass('show-webcam')
	                    root.stream = stream
	                    video.src = window.URL.createObjectURL(stream)
	                    video.play()

	                }, errCallBack)
	            }
				// Legacy code below: getUserMedia
	            // else if (navigator.getUserMedia) { // Standard
	            //     navigator.getUserMedia({ video: true }, function(stream) {
	            //         video.src = stream;
	            //         video.play()
	            //     }, errCallBack)
	            // } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
	            //     navigator.webkitGetUserMedia({ video: true }, function(stream){
	            //         video.src = window.webkitURL.createObjectURL(stream);
	            //         video.play()
	            //     }, errCallBack)
	            // } else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
	            //     navigator.mozGetUserMedia({ video: true }, function(stream){
	            //         video.src = window.URL.createObjectURL(stream);
	            //         video.play();
	            //     }, errCallBack)
	            // }
            },

            _handleWebcamCapture: function(e) {
				e.preventDefault()
				
                let canvasWidth =  root.$canvas.width(),
                    canvasHeight =  root.$canvas.height(),
                    canvasTemp = document.createElement('canvas')

                canvasTemp.width = 640
                canvasTemp.height = 480

                var tempCtx = canvasTemp.getContext("2d")
	            tempCtx.drawImage(root.$video, 0, 0)

	            var dataURL = canvasTemp.toDataURL('image/jpeg')
				
				root._handleWebcamClose()
				root._readImage(dataURL, dataURL, {name: 'webcam', type: 'image/jpeg'})
            },

            _handleWebcamClose: function(e) {
            	if(e) e.preventDefault()
				
				var track = root.stream.getTracks()[0]
				track.stop()
				root.$photoEditor.removeClass('show-webcam')
            },

            _handleMenu: function(e) {
                e.preventDefault()
                var menu = $(this).attr('data-menu')
                
                root._changeRoute(menu)
            },

            _paginate: function(e) {
            	e.preventDefault()
				
				if (!$(this).hasClass('disabled')) {
 	            	var paginate = $(this).attr('data-paginate'),
	            		route = 0,
	            		currentRoute = root.routeCurrentIndex,
	            		routesLength = root.routes.length
	        
	                if (paginate === 'next') 
	                	route = currentRoute += 1
	                else 
	                	route = currentRoute -= 1
					
					if (route >= 0 && route <= routesLength) {
						var menu = root.routes[route]
						root._changeRoute(menu)
					}
				}
            },

            _enablePaginationButton: function(isBack, isNext) {
				root.$pagination.find('li').addClass('disabled')
				isBack && root.$paginateBackBtn.removeClass('disabled')
				isNext && root.$paginateNextBtn.removeClass('disabled')
            },                 

            _changeRoute: function(menu) {
            	root._log('changed menu route: ', menu)

                if (menu !== 'take-photo') {
                    if (!root.image) return false
                }

            	var routeCurrentIndex = root.routes.indexOf(menu);
				
				// add active class
                root.$menus.removeClass('active')
                root.$menu.find('.menu-' + menu).addClass('active')
				
				// add indicator class
				var classes = 'page-' + root.routes.join(' page-')
                root.$photoEditor.removeClass(classes).addClass('page-' + menu)
				
				// if mobile then, show only 2 consecutive menu only
				if (root.$photoEditor.hasClass('mobile')) {
					root.$menus.removeClass('show')
					root.$menu.find('.menu-' + menu).addClass('show')
					
					if ((routeCurrentIndex + 1) === root.routes.length) {
						let menu = root.routes[routeCurrentIndex - 1]
						root.$menu.find('.menu-' + menu).addClass('show')
					} else {
						if (root.routes[routeCurrentIndex + 1]) {
							let menu = root.routes[routeCurrentIndex + 1]
							root.$menu.find('.menu-' + menu).addClass('show')
						}
					}
				}

                root.$tools.find('.tool').removeClass('show')
				
				// change current route
				root.routeCurrentIndex = routeCurrentIndex
				

				if (root.routeCurrentIndex <= 0) {
					// disable 'back' and enable 'next' pagination btn
					root._enablePaginationButton(false, true)
				} else if (root.routeCurrentIndex >= (root.routes.length - 1)) {
					// enable 'back' and disable 'next' pagination btn
					root._enablePaginationButton(true, false)
				} else {
					// enable 'back' and enable 'next' pagination btn
					root._enablePaginationButton(true, true)
				}
				

				switch (menu) {
					case 'take-photo': 
						// clear 
						root.$canvas.find('img.frame').remove()
						root._showTool(menu)
						break;
					case 'select-frames': 
						root._showTool(menu)
						var key = 0;

						// check if user has filter selected, then select it
						// if (root.frameSelectedKey)
						// 	key = root.frameSelectedKey

						root.$frames.find('[data-slick-index="'+ key +'"]').trigger('click')

						break;
					case 'edit-photo': 
						root._showTool(menu)
						break;
					case 'submit': 
						root._showTool(menu)
						break;
				}
            },

            _handleEditPhotoTools: function(e) {
				e.preventDefault()
				var subtool = $(this).attr('data-subtool')
                
                root.$toolEditPhoto.find('.tool-btn.active').removeClass('active')
                $(this).addClass('active')
                root.$toolEditPhoto.find('.sub-tool').removeClass('show')
                root.$toolEditPhoto.find('.sub-tools .' + subtool).addClass('show')
            },

            _showTool: function(tool) {
            	root._log('select tool: ' + tool)
                root.$tools.find('.tool').removeClass('show')
                root.$tools.find('.tool-' + tool).addClass('show')
				

				// SLICK HACK PART
				if (root.$frames.hasClass('slick-initialized')) {
					root.$frames.slick('unslick')
				}

				if (tool === "select-frames") {
					// root.$frames.slick('reinit')
					// root.$frames.slick('slickGoTo', 3)

					// enable slick slider
					root.$frames.slick({
						infinite: false,
						slidesToShow: 3,
						slidesToScroll: 3,
						nextArrow: '#frames-slider-next-arrow',
						prevArrow: '#frames-slider-prev-arrow'
					})
				}
            },

            _selectFrame: function(e) {
                e.preventDefault()
            	root._log('select frame..')
                
                var key = $(this).attr('data-key')
				
				// clear frames
				root.$canvas.find('img.frame').remove()

                // add and remove selected class
                root.$frames.find('li.selected').removeClass('selected')
                $(this).addClass('selected')

                root._attachFrame(key)
            },

            _attachFrame: function(key) {
            	root._log('attach frame..')
            	root.frameSelectedKey = key

				var src = root.frames[key].img,
					imgElem = $('<img src="' + src + '" class="frame"/>')

				root.frame = $(imgElem)
				            .clone()
				            .bind('mousedown touchstart', function(event) { root._imageDrag(event)});

				// show the image
				root.$canvas.append(root.frame)
            },
	
            _selectFile: function(e) {
                e.stopPropagation()
                e.preventDefault()
                
                // clear canvas and reset some value
                root._clearCanvas()

                // enable page arrow indicator
				root.$pageArrowIndicator.removeClass('disabled')

                // show preloader
                root._showPreloader(1)

                var files = e.target.files,
                    result = '',
                    file
				
				if (files.length) {
	                

	                for (var i = 0; file = files[i]; i++) {
	                    // if the file is not an image, continue
	                    if (!file.type.match('image.*')) {
	                    	// hide preloader
	                    	root._showPreloader(0)
	                        break
	                    }
	            
	                    var reader = new FileReader()
	                    reader.onload = (function (tFile) {
	                        return function (e) {
	                        	if (tFile.name) {
	                        		root.$fileInputText.val(tFile.name)
	                        	}

            	                loadImage(files[0], function (img) {
                                    var dataURL = img.toDataURL('image/jpeg')

                                    root._readImage(dataURL, dataURL, tFile)
            	                }, { // options
            						orientation: true,
            						canvas: true
            	                })
	                        }
	                    }(file))

	                    reader.readAsDataURL(file)
	                }
	            } else {
	            	// hide preloader
	            	root._showPreloader(0)
	            }
            },

            _readImage: function(image, src, info) {
            	var canvas = root.$canvas,
                    img = new Image
                
                img.src = image

                img.onload = function(tmp) {
                    var width = img.width,
                        height = img.height,
                        { top, left, useWidth, useHeight, ratio } = root._centerImage(width, height)

                    // Prepare Images
                    var imgElem = $('<img src="' + src + '" name="' + info.name + '" />')
                        imgElem.css({left: left, top: top, width: useWidth, height: useHeight })

                    root.image = $(imgElem)
                                .clone()
                                .data({ 
                                    mime: info.type, 
                                    width: width, 
                                    height: height, 
                                    ratio: ratio, 
                                    left: left, 
                                    top: top, 
                                    useWidth: useWidth, 
                                    useHeight: 
                                    useHeight
                                })
                                .addClass('main')
                                .bind('mousedown touchstart', function(event) { root._imageDrag(event)});
					
                    // place the image
                    root.$canvas.append($('<div class="wrapper"></div>').append(root.image))
					
					// disable page arrow indicator
					root._handlePageArrowIndicator(false)

					// check if need to show ghost image
                    if (root.options.showCopy) {
                    	root.imageCopy = $(imgElem).addClass('copy')
                    	root.$canvas.append(root.imageCopy)
                    }

                    // disable 'back' and enable 'next' pagination btn
					root.$pageArrowIndicator.addClass('disabled')

                    // hide preloader
                	root._showPreloader(false)
                }
            },

            _imageDrag: function(e) {
                e.preventDefault() // disable selection
                root._log('dragging..')
                
                var event = (e.originalEvent.touches || e.originalEvent.changedTouches) ? e.originalEvent.touches[0] || e.originalEvent.changedTouches[0] : e,
                    canvas = root.$canvas,
                    image = this.image,
                    height = image.outerHeight(),
                    width = image.outerWidth()

                // if image is rotated to landscape 
                if(root._isImageRotated())
                	[height, width] = [width, height] // swap values

                var cursorY = image.offset().top + height - event.pageY,
                    cursorX = image.offset().left + width - event.pageX
           
                $(document).
                    on({'dragstart mousemove touchmove': function(e) {
                        e.stopImmediatePropagation()
                        e.preventDefault()

                        var event = (e.originalEvent.touches || e.originalEvent.changedTouches) ? e.originalEvent.touches[0] || e.originalEvent.changedTouches[0] : e,
                            imgTop = event.pageY + cursorY - height,

                            imgLeft = event.pageX + cursorX - width
                        
                        if(parseInt(imgTop - $(canvas).offset().top) > 0) { 
                            imgTop = $(canvas).offset().top
                        } else if (imgTop + height < $(canvas).offset().top + $(canvas).outerHeight()) {
                            imgTop = $(canvas).offset().top + $(canvas).outerHeight() - height 
                        }
                             
                        if (parseInt(imgLeft - $(canvas).offset().left) > 0) {
                            imgLeft = $(canvas).offset().left
                        } else if (imgLeft + width < $(canvas).offset().left + $(canvas).outerWidth()) {
                            imgLeft = $(canvas).offset().left + $(canvas).outerWidth() - width
                        }
                        
                        image.offset({
                            top: imgTop,
                            left: imgLeft
                        })
                        
                        root._imageCopy()
                    }, 'dragend mouseup touchend': function() {
                        root._imageUnDrag()
                    }
                })
            },

            _imageUnDrag: function() {
                $(document).unbind('mousemove touchmove')
                root.$body.css({ cursor:'' })
            },

            _imageCopy: function() {
            	// check if need to show ghost image
                if (!root.options.showCopy) return
                
                var image = root.image,
                    imageCopy = root.imageCopy

                imageCopy.css({
                    width: image.css('width'), 
                    height: image.css('height'), 
                    transform: 'rotate('+ root.rotateDeg +'deg)',
                    top: image.css('top'), 
                    left: image.css('left')
                })
            },

			_handleRotateBtn: function() {
                if (!root.image) return false
				
				var deg = root.rotateDeg,
					rotate = $(this).attr('data-rotate')

				if (rotate === 'clockwise')
					deg += 90
				else
					deg -= 90

                root._rotateImage(deg)
            }, 

            _centerImage: function(useWidth, useHeight) {
            	var canvas = root.$canvas,
            		ratio = useWidth / useHeight,
            		canvasRatio = canvas.outerWidth() / canvas.outerHeight()

				if (root.options.resizeImageAfterUpload) {
					useWidth = canvas.outerWidth() + 40;
					useHeight = useWidth / ratio;
					
					if (useHeight < canvas.outerHeight()) {
						useHeight	= canvas.outerHeight() + 40;
						useWidth	= useHeight * ratio;
					}
				// check if the image dimesion is smaller than canvas
				} else if (useWidth < canvas.outerWidth() || useHeight < canvas.outerHeight()) { 
                           
                    if (ratio < canvasRatio) {
                        useWidth    = canvas.outerWidth()
                        useHeight   = useWidth / ratio
                    } else {
                        useHeight   = canvas.outerHeight()
                        useWidth    = useHeight * ratio
                    }
                }

                // center the image on canvas
                var left = parseFloat((canvas.outerWidth() - useWidth) / 2),
                    top = parseFloat((canvas.outerHeight() - useHeight) / 2)

                return {
					top,
					left,
					useWidth,
					useHeight,
					ratio
                }
            },

            _rotateImage: function(deg) {
        		if (deg < 0) deg = 270
            	if (deg >= 360) deg = 0
				
				root.image.css({ transform: 'rotate('+ deg +'deg)' })

            	// save in global variable
            	root.rotateDeg = deg
				root._repositionImage()
            },

            _repositionImage: function() {
            	root._log('reposition..')

            	// reposition the image in canvas
	    		var canvas = root.$canvas,
	    			image = root.image,
	    			imgHeight = image.outerHeight(),
	    	        imgWidth = image.outerWidth(),
	    	        imgOffsetTop = image.offset().top,
				    imgOffsetLeft = image.offset().left				    

				// if image is rotated to landscape 
            	if (root._isImageRotated())
					[imgHeight, imgWidth] = [imgWidth, imgHeight]  // swap values

				var canvasTop = canvas.offset().top,
				    canvasLeft = canvas.offset().left,
				    canvasHeight = canvas.outerHeight(),
				    canvasWidth = canvas.outerWidth(),
				    imgTop = image.css('top'),
				    imgLeft = image.css('left')

				if (imgOffsetTop > canvasTop) {
					imgTop = canvasTop
				} else {
					// if image is less than the bottom part of the canvas, then reposition it
					if ((imgOffsetTop + imgHeight) < (canvasTop + canvasHeight))
						imgTop = canvasTop + canvasHeight - imgHeight
				}

				if (imgOffsetLeft > canvasLeft) {
					imgLeft = canvasLeft
				} else {
					// if image is less than to the right side of canvas, then reposition it
					if ((imgOffsetLeft + imgWidth) < (canvasLeft + canvasWidth))
						imgLeft = canvasLeft + canvasWidth - imgWidth 
				}

				image.offset({
				    top: imgTop,
				    left: imgLeft
				})

				root._imageCopy()
            },

            _resizeImage: function() {
                if (!root.image) return false

                root.resizeValue = parseInt($(this).find('.slider-handle').attr('aria-valuenow'))
                root._zoomImage(2)
            }, 

            _zoomImage: function(x){
            	root._log('zoom..')

                if (!root.image) {
                    root._clearTimers()
                    return false
                }

                var canvas = root.$canvas,
                	canvasTop = canvas.offset().top,
				    canvasLeft = canvas.offset().left,
				    canvasOuterWidth = canvas.outerWidth(),
				    canvasOuterHeight = canvas.outerHeight(),
                    image = root.image,
                    imageOuterHeight = image.outerHeight(),
                    imageOuterWidth = image.outerWidth(),
                    ratio = image.data('ratio'),
                    newWidth = imageOuterWidth + x,
                    newHeight = newWidth / ratio,
                    imageTop = image.css('top').replace(/[^-\d\.]/g, ''),
                    imageLeft = image.css('left').replace(/[^-\d\.]/g, '')
				

				// if already zoom out, dont do anything
				if ((x < 0) && (imageOuterHeight <= canvasOuterHeight || imageOuterWidth <= canvasOuterWidth)) return

                // smaller than canvas width?
                if (newWidth < canvasOuterWidth) {
                    newWidth = canvasOuterWidth
                    newHeight = newWidth / ratio
                }

                // smaller than canvas height?
                if (newHeight < canvasOuterHeight) {
                    newHeight = canvasOuterHeight
                    newWidth = newHeight * ratio
                }
                
                var newTop  = imageTop - (newHeight - imageOuterHeight) / 2
                var newLeft = imageLeft - (newWidth - imageOuterWidth) / 2
                
                if ((canvasLeft - newLeft) < canvasLeft) {
                    newLeft = 0
                    if (root._isImageRotated())
                    	newLeft = imageLeft - (newWidth - imageOuterWidth) / 2
                } else if (canvasOuterWidth > (newLeft + imageOuterWidth) && x <= 0) {
                    newLeft = canvasOuterWidth - newWidth
                    if (root._isImageRotated())
                    	newLeft = imageLeft - (newWidth - imageOuterWidth) / 2
                }
                
                if ((canvasTop - newTop) < canvasTop) {
                    newTop = 0
                } else if (canvas.outerHeight() > (newTop + imageOuterHeight) && x <= 0) {
                    newTop = canvas.outerHeight() - newHeight
                }

                // add limit to zoom in, check if the zoom value is double the image orig size
                var limitWidth = image.data('useWidth') * 2,
                    limitHeight = image.data('useHeight') * 2

                if (newWidth <= limitWidth) {

                    image.css({
                        width: newWidth,
                        height: newHeight, 
                        top: newTop,
                        left: newLeft 
                    })

                    if (root._isImageRotated())
                    	root._repositionImage()
                    else 
                    	root._imageCopy()
                }
            },

            _saveImage: function() {
                if (!root.image) return false
				
				root._showPreloader(true)

                var _self = root,
                    canvas = root.$canvas,
                    canvasOffsetTop = canvas.offset().top,
                    canvasOffsetLeft = canvas.offset().left,
                    image = root.image,
                    imageSrc = image.attr('src'),
                    input = root.input,
                    options = root.options,
                    factor  = (options.width != canvas.outerWidth()) ? options.width / canvas.outerWidth() : 1,
                    finalWidth = options.width, 
                    finalHeight = options.height, 
                    finalTop = parseInt(Math.round(parseInt(image.css('top')) * factor)),
                    finalLeft = parseInt(Math.round(parseInt(image.css('left')) * factor)),
                    imageWidth = parseInt(Math.round(image.width() * factor)),
                    imageHeight = parseInt(Math.round(image.height() * factor)),
                    imageOffsetTop = image.offset().top,
                    imageOffsetLeft = image.offset().left,
                    imageOriginalWidth = image.data('width'),
                    imageOriginalHeight = image.data('height'),
                    rotateDeg = root.rotateDeg,
                    imageFinalType = options.imageType

                if (root._isImageRotated()) {
	                finalLeft = (imageOffsetLeft - canvasOffsetLeft) * factor 
                    finalTop = (imageOffsetTop - canvasOffsetTop) * factor  
                }

                var finalData = {
                	frame: root.frames[root.frameSelectedKey],
                    name: image.attr('name'), 
                    //imageOrigInfo: image, 
                    canvasOffsetTop,
                    canvasOffsetLeft,
                    imageOriginalWidth, 
                    imageOriginalHeight, 
                    imageOffsetTop,
                    imageOffsetLeft,
                    imageWidth, 
                    imageHeight, 
                    finalWidth, 
                    finalHeight, 
                    finalLeft, 
                    finalTop,
                    rotateDeg,
                    imageFinalType
                }

                // create the final canvas result
                var canvas = document.createElement('canvas')
                    canvas.width = finalWidth
                    canvas.height = finalHeight
                    
                var ctx = canvas.getContext("2d")

                if (root.options.backgroundColor === 'transparent') {
                    // transparent background color
                    ctx.clearRect(0, 0, finalWidth, finalHeight)
                } else {
                    // with custom background color
                    ctx.fillStyle = root.options.backgroundColor
                    ctx.fillRect(0, 0, finalWidth, finalHeight)
                }

                // check if image is rotated
                if (rotateDeg > 0) {
                    var img0  = new Image()
                        img0.src = image.attr('src')
				
                    img0.onload = function() {
                    	if (root._isImageRotated())
                    		[imageOriginalWidth, imageOriginalHeight] = [imageOriginalHeight, imageOriginalWidth]

                        var canvas0 = document.createElement('canvas')
                            canvas0.width = imageOriginalWidth
                            canvas0.height = imageOriginalHeight
                        
                        var ctx0 = canvas0.getContext("2d"),
                            w = imageOriginalWidth / 2,
                            h = imageOriginalHeight / 2 

                        if (rotateDeg === 90)
                            h = imageOriginalWidth / 2
						
						if (rotateDeg === 270)
                            w = imageOriginalHeight / 2,
	
                        ctx0.clearRect(0, 0, imageOriginalWidth, imageOriginalHeight)
                        ctx0.save()
                        ctx0.translate(w, h)
                        ctx0.rotate(rotateDeg * Math.PI/180)
                        ctx0.drawImage(img0, -w, -h)
                        ctx0.restore()

                        // rotated image result
                        imageSrc = canvas0.toDataURL(imageFinalType)

                        // let's crop the image
                        root._cropImage(canvas, ctx, imageSrc, imageFinalType, imageWidth, imageHeight, finalWidth, finalHeight, finalLeft, finalTop, finalData)
                    }
                } else {
                    // for better performance, if image is not rotated then let's crop it directly
                    root._cropImage(canvas, ctx, imageSrc, imageFinalType, imageWidth, imageHeight, finalWidth, finalHeight, finalLeft, finalTop, finalData)
                }
            },

            _cropImage: function(canvas, ctx, imageSrc, imageFinalType, imageWidth, imageHeight, finalWidth, finalHeight, finalLeft, finalTop, finalData) {
            	var dataURL,
                	img  = new Image()
                    img.src = imageSrc
                
                if (root._isImageRotated()) {
                	[imageWidth, imageHeight] = [imageHeight, imageWidth]
                }	

                //root._byPass(imageSrc, 1)

                // prepare image, resize it
                img.onload = function() {
                    var canvasTemp = document.createElement('canvas')
                        canvasTemp.width = imageWidth
                        canvasTemp.height = imageHeight

                    var tempCtx = canvasTemp.getContext("2d")
                        tempCtx.clearRect(0, 0, imageWidth, imageHeight)
                        tempCtx.drawImage(img, 0, 0, imageWidth, imageHeight)

                    var tempImg = new Image()
                        tempImg.src = canvasTemp.toDataURL(imageFinalType)

                		//root._byPass(tempImg.src, 2)

                		// crop image in canvas
                        tempImg.onload = function() {
                        	if (imageWidth < finalWidth || imageHeight < finalHeight)
                        		ctx.drawImage(tempImg, finalLeft, finalTop, imageWidth, imageHeight)
                        	else
                        	    ctx.drawImage(tempImg, finalLeft*-1, finalTop*-1, finalWidth, finalHeight, 0, 0, finalWidth, finalHeight)
                           
                            
                            var tempImg2 = new Image()
                                tempImg2.src = root.frames[root.frameSelectedKey].img

                            //root._byPass($(canvas)[0].toDataURL(imageFinalType), 3)

							// Add Frame
                            tempImg2.onload = function() {
                                ctx.drawImage(tempImg2, 0, 0, finalWidth, finalHeight, 0, 0, finalWidth, finalHeight)

                                // get the cropped image
                                dataURL = $(canvas)[0].toDataURL(imageFinalType)

                                // send final data and image
                                root._sendImage(dataURL, finalData, imageFinalType) 
                                //root._byPass($(canvas)[0].toDataURL(imageFinalType), 4)
                            }
                        }
                }
            },

            _sendImage: function(base64Img, finalData, imageFinalType) {
				var url = root.$configURL.val(),
					token = root.$configToken.val(),
					data = JSON.stringify(finalData)
				
				$.ajax({
			        url: url, 
			        type: 'POST',
			        data: { data, 'csrf_test_name': token, 'image': base64Img },
			        dataType: 'json',
			        beforeSend: function(res){},
			        complete: function(res){},
			        success: function(res){
            			// hide preloader
        				root._showPreloader(false)

        				if (res.error == 0) {
        					// show page success
        					root._showPageSuccess(res, base64Img)
        				} else {
							root.$configToken.val(res.auth_token_hash)
							alert(res.mesage)
        				}
        				
			        }
			    }); //end ajax
            },

            _resetImage: function(e) {
                e.preventDefault()
                if (!root.image) return false

                var image = root.image

                // reset rotate
                root.rotateDeg = 0
                //root.$rotateSlider.find('.slider-handle').css({ left: 0 })
                //root.$rotateSlider.find('.slider-handle').attr('aria-valuenow', 0)
                //root.$rotateSlider.find('.slider-fill').css({ width: 0 })
                root.$canvas.find('img').css('transform', 'rotate(0deg)')

                // reset resize
                root.resizeValue = 0
                //root.$resizeSlider.find('.slider-handle').css({ left: 0 })
                //root.$resizeSlider.find('.slider-fill').css({ width: 0 })
                //root.$resizeSlider.find('.slider-handle').attr('aria-valuenow', 0)
				
				// reset frame
                root.frameSelectedKey = null

                image.css({
                    width: image.data('useWidth'), 
                    height: image.data('useHeight'), 
                    top: image.data('top'), 
                    left: image.data('left')
                })

                root._imageCopy()
                root._changeRoute('take-photo')
            },

            _clearCanvas() {
            	root.$photoEditor.removeClass('show-webcam show-webcam-error')
            	root.$canvas.find('.wrapper').remove()
                root.$canvas.find('img.main').remove()
                root.$canvas.find('img.copy').remove()
                root.$canvas.find('img.frame').remove()
                root.$fileInputText.val('')
                root.rotateDeg = 0
                root.resizeValue = 0
                root.frame = null
                root.image = null
                root.imageCopy = null
                root.interval = null
                root.frameSelectedKey = null
            },

            _clearTimers: function() {
                // make sure to clear all timers
                var interval_id = window.setInterval("", 9999)

                for (var i = 1; i < interval_id; i++)
                    window.clearInterval(i)
            },

            _showPreloader: function(isShow) {
				if(isShow)
					root.$photoEditor.addClass('loading').find('img.copy').hide()
				else 
					root.$photoEditor.removeClass('loading').find('img.copy').show()
            },

            _showModal: function(msgs) {
            	if (msgs)
                	root.$modalOverlay.css('display', 'flex').find('.mesages').html(msgs)
               	else 
               		root.$modalOverlay.hide().find('.mesages').html('')
            },

            _showPageSuccess: function(res, base64Img) {
            	// change route
            	root._changeRoute('confirmation')

				// append the share urls
				//root.$shareFacebookbtn.attr('href', res.data.fb_share_url)
				//root.$shareTwitterbtn.attr('href', res.data.twitter_share_url)
				root.$shareBtn.attr('href', res.data.view_details_url)


                // display generated image - res.data.image_url
                root.$resultPhoto.html(`<img src="${base64Img}" />`)  

                // show success page
				root.$photoEditor.addClass('page-success')
            },

            _isImageRotated: function() {
            	return root.rotateDeg === 90 || root.rotateDeg === 270
            },

            _log(msgs) {
            	//console.log(msgs)
            },

            _byPass(dataURL, id) {
                /////////////// BY PASS
				root._showPreloader(0)
				$('.result-photo-bypass').show()
                $('#result' + id).html(`<img src="${dataURL}" />`)
            	//root._changeRoute('confirmation') 
				//root.$photoEditor.addClass('page-success') 
            }
        }

    $(function(){
        photoEditor.init()
    })
})(jQuery)
