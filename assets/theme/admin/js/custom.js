
//Admin JS
(function($){

   $(function(){

       var app = {

           'gallery_form' :{
               'arr_status':[1,0],
               'arr_returnButtonText':["Hide","Show"],
               'arr_returnStatus' :["Active","Not Active"],
               'currentButton': '',
               'change_status':function(elem){

                   var buttonHandle = $(elem);
                   var $this = this;

                   if(buttonHandle.length){

                       var button = buttonHandle.find('a.toggle');

                       button.on('click',function(e){
                                e.preventDefault();

                                $this.currentButton = $(this);
                                app.gallery_form.updateStatus('.list-item-'+ $(this).attr('data-entry-id'));


                       });
                   }
               },
               'modal':function (btnElem) {

                   var $this = this;
                   var buttonModal = $(btnElem);
                   var galleryModal = $("#galleryModal");

                   if(buttonModal.length){
                       buttonModal.on('click',function(e){
                           e.preventDefault();

                           var parentElem = $(this).closest('.list-item-'+ $(this).attr('data-entry-id')),
                               status = parentElem.attr('data-option-status'),
                               entry_id = parentElem.attr('data-entry-id'),
                               data = parentElem.data('details'),
                               image_url = '<img width="500" src="' + parentElem.attr('data-image-url') + '">';
                            console.log(data);
                           //show modal
                           galleryModal.modal();
                           galleryModal.find('.image-wrapper').html(image_url);
                           galleryModal.find('.modal-title').find('.id').html(entry_id);
                           galleryModal.find('.modal-title').find('.status').html($this.arr_returnStatus[$this.arr_status[status]]);
                           galleryModal.find('.toggle').html('<strong>' + $this.arr_returnButtonText[$this.arr_status[status]] + '</strong>');
                           galleryModal.find('.toggle').attr('data-entry-id',entry_id);

                           //details
                           galleryModal.find('.info-handle .photo_id span').html(data.entry_id);
                           galleryModal.find('.info-handle .first_name span').html(data.first_name);
                           galleryModal.find('.info-handle .last_name span').html(data.last_name);
                           galleryModal.find('.info-handle .email span').html(data.email);
                           galleryModal.find('.info-handle .complete_address span').html(data.address);
                           galleryModal.find('.info-handle .regional_address span').html(data.region);
                           galleryModal.find('.info-handle .dob span').html(data.formatDob); 
                           galleryModal.find('.info-handle .mobile_number span').html(data.mobile_number);

                       })
                   }

                   var btnModal = galleryModal.find('button.toggle');

                   btnModal.on('click',function(){

                       $this.currentButton = $(this);
                       app.gallery_form.updateStatus('.list-item-'+ $(this).attr('data-entry-id'));

                   });
               },
               'updateStatus':function(className){

                   var $this = this;
                   parentContainer = $('#gallery-container');
                   parentElem = $(className);

                   anchorStatus = parentElem.attr('data-option-status');
                   anchorId = parentElem.attr('data-entry-id'),
                   requestUrl = parentElem.attr('data-action-url'),
                   statusLabel = parentElem.find('.status-label'),
                   csrfToken = parentContainer.attr('data-csrf-token'),
                   modalElem = $this.currentButton.closest('#galleryModal');
                   var request = $.ajax({
                       type:'post',
                       url: requestUrl,
                       data: { entry_id:anchorId, status:$this.arr_status[anchorStatus],csrf_test_name:csrfToken},
                       beforeSend: function () {
                           $this.currentButton.html('<i class="fa fa-spinner fa-pulse "></i>');
                       },
                       success:function(data){
                           if(data.code == 0){

                               var buttonText = '<strong>' + $this.arr_returnButtonText[anchorStatus] + '</strong>';

                               parentElem.attr('data-option-status',$this.arr_status[anchorStatus]);
                               statusLabel.html($this.arr_returnStatus[anchorStatus]);
                               parentElem.find('a.toggle').html(buttonText);
                               $this.currentButton.html(buttonText);
                               parentContainer.attr('data-csrf-token',data.csrf_token);
                               var modalStatus = modalElem.find('.status');
                               if(modalStatus.length){
                                   modalStatus.html($this.arr_returnStatus[anchorStatus]);
                               }
                           }
                       },
                       error: function(){
                           $this.currentButton.html($this.arr_returnButtonText[anchorStatus]);
                       }
                   })


               },
               'selectStatus':function(selector){

                   var elemSelector = $(selector);

                   if(elemSelector.length){
                       elemSelector.on('change',function (e) {
                           select = $(this);
                           
                           var request = $.ajax({
                               type:'post',
                               url: select.attr('data-request-url'),
                               data: { filter_status:select.val(),csrf_test_name:select.attr('data-csrf-token')},
                               success:function(data){
                                   window.location.href = select.attr('data-base-url');
                               },

                           })

                       })
                   }
               },
               'exportTo': function(selector){

                   var elemSelector = $(selector);

                   if(elemSelector.length){
                       elemSelector.on('change',function (e) {
                           select = $(this);
                           if(select.val() != ""){
                               window.location.href = select.attr('data-base-url') + select.val();
                           }
                       });
                   }
               },
           },
           'toggleForm':function(checkBox){

               var checkBoxBtn = $(checkBox);
                var inputPassword = $('#password');
               if(checkBoxBtn.length){
                   checkBoxBtn.on('change',function(){
                        if(this.checked){
                            inputPassword.prop('required',true);
                        }else{
                            inputPassword.prop('required',false);
                        }
                   });
               }
           },
           'init':function () {
                app.gallery_form.change_status('.option-status');
                app.gallery_form.modal('.btn-modal');
                app.gallery_form.selectStatus('#selectStatus');
                app.gallery_form.exportTo('#exportTo');
                app.toggleForm('#chage_password');
           }
       };


       app.init();


   });
})(jQuery);